import javax.swing.*;
import java.util.ArrayList;

/**
 * Created by SoOreck on 15/10/2016.
 */
public class Pila {

    private ArrayList<Integer> pila;
    private ArrayList<String> variables;

    public Pila(){
        pila = new ArrayList<Integer>();
        variables = new ArrayList<String>();
    }

    public boolean isEmpty(){
        return pila.isEmpty();
    }

    public void push(int valor){
        pila.add(valor);
    }

    public int pop(){
        int last;
        last = pila.remove((pila.size()-1));
        return last;
    }

    public int lastValue(){
        return (pila.get((pila.size()-1))) ;
    }

    public int length(){
        return pila.size();
    }

    public int get(int i){
        return pila.get(i);
    }
    public void clear(){
        pila.clear();
    }

    public void pushString(String valor){
        this.variables.add(valor);
    }

    public String popString(){
        String last;
        last = this.variables.remove((this.variables.size()-1));
        return last;
    }

    public String lastValueString(){
        return (variables.get((this.variables.size()-1))) ;
    }

    public int lengthString(){
        return this.variables.size();
    }

    public String getString(int i){
        return this.variables.get(i);
    }
    public void clearString(){
        this.variables.clear();
    }

    /************************************************ METODOS PILA STRING *********************************************/



    /****************************************** METODOS DE PILA PARA SINTACTICO ***************************************/

    public void PUSH(int valor, JTextArea edo){
        pila.add(valor);
        edo.append("\nDato PUSH: "+valor);
    }

    public int POP( JTextArea edo){
        int last;
        last = pila.remove((pila.size()-1));
        edo.append("\nDato POP: "+last);
        return last;
    }

    public int LastValue(){
        return (pila.get((pila.size()-1))) ;
    }
}
