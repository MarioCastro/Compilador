import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by SoOreck on 20/10/2016.
 */

public class ModelTableCuadruplos extends AbstractTableModel {
    public List<Cuadruplo> cuadruplos = new ArrayList<Cuadruplo>();
    String [] encabezado = {"apuntador","cod. operador","op1","op2","resultado"};


    public ModelTableCuadruplos(){

    }

    public void add(Cuadruplo cuadruplo) {
        cuadruplo.setApuntador(cuadruplos.size()+1);
        cuadruplos.add(cuadruplo);
        fireTableDataChanged();
    }

    public void remove(int rowIndex) {
        cuadruplos.remove(rowIndex);
        fireTableDataChanged();
    }

    public void clean() {
        cuadruplos.clear();
        fireTableDataChanged();
    }

    public List regresa(){return cuadruplos;}



    @Override
    public int getRowCount() {
        return cuadruplos.size();
    }

    @Override
    public int getColumnCount() {
        return encabezado.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex){
            case 0: return cuadruplos.get(rowIndex).getApuntador();
            case 1: return cuadruplos.get(rowIndex).getCodoperS();
            case 2: return cuadruplos.get(rowIndex).getOp1S();
            case 3: return cuadruplos.get(rowIndex).getOp2S();
            case 4: return cuadruplos.get(rowIndex).getResS();
        }
        return null;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        cuadruplos.get(rowIndex).setResS(aValue.toString());
        super.setValueAt(aValue, rowIndex, columnIndex);
        fireTableDataChanged();
    }

    @Override
    public String getColumnName(int columnIndex) {
        return encabezado[columnIndex];
    }

}
