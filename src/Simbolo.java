import java.io.Serializable;

/**
 * Created by SoOreck on 19/10/2016.
 */
public class Simbolo implements Serializable {

    private int id;
    private int variable;
    private String descrip;
    private int tipo;
    private int ram;
    private int apuntador;
    private String tipoS;

    private String valor;

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Simbolo(int id, int variable, String descrip, int tipo, String tipoS, int ram, int apuntador){
        this.id = id;
        this.variable = variable;
        this.descrip = descrip;
        this.tipo = tipo;
        this.ram = ram;
        this.apuntador = apuntador;
        this.tipoS = tipoS;
        this.valor = "";

    }

    public Simbolo(){
    }

    public String getTipoS() {
        return tipoS;
    }

    public void setTipoS(String tipoS) {
        this.tipoS = tipoS;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVariable() {
        return variable;
    }

    public void setVariable(int variable) {
        this.variable = variable;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public int getRam() {
        return ram;
    }

    public void setRam(int ram) {
        this.ram = ram;
    }

    public int getApuntador() {
        return apuntador;
    }

    public void setApuntador(int apuntador) {
        this.apuntador = apuntador;
    }
}
