/**
 * Created by SoOreck on 27/10/2016.
 */
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;



public class ModelTableConstantes extends AbstractTableModel {

    private List<Simbolo> simbolos = new ArrayList<Simbolo>();
    String [] encabezado = {"dir.","variable","descrip.","tipo","RAM","apuntador"};


    public ModelTableConstantes(){

    }

    public void add(Simbolo simbolo) {
        simbolos.add(simbolo);
        fireTableDataChanged();
    }

    public void remove(int rowIndex) {
        simbolos.remove(rowIndex);
        fireTableDataChanged();
    }

    public void clean() {
        simbolos.clear();
        fireTableDataChanged();
    }

    public List<Simbolo> regresa(){
       return simbolos;
    }

    public Simbolo get(int i){
        return simbolos.get(i);
    }



    @Override
    public int getRowCount() {
        return simbolos.size();
    }

    @Override
    public int getColumnCount() {
        return encabezado.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex){
            case 0: return simbolos.get(rowIndex).getId();
            case 1: return simbolos.get(rowIndex).getVariable();
            case 2: return simbolos.get(rowIndex).getDescrip();
            case 3: return simbolos.get(rowIndex).getTipoS();
            case 4: return simbolos.get(rowIndex).getRam();
            case 5: return simbolos.get(rowIndex).getApuntador();
        }
        return null;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return encabezado[columnIndex];
    }




}