import javafx.scene.control.Tab;
import jdk.nashorn.internal.scripts.JO;
import sun.util.resources.cldr.aa.CurrencyNames_aa;

import javax.swing.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;

public class Sintactico {

    /*Al momeno de generar cuadruplos se usan estos datos enteros para representar lo siguiente:
    * -1: Parte del cuadruplo que esta vacio
    * 1:  Parte del cuadruplo que corresponde a un GOTO
    * 2:  Parte del cuadruplo que corresponde a un GOTOF
    * 3:  Parte del cuadruplo que corresponde a un GOTO
    * 0:  Parte del cuadruplo que representa una direccion pendiente por resolver
    * 909: PRINT - OUTPUT
    * 908: WRITE - INPUT */

    Tablas tablas;
    int startPila;
    int finalPila;
    int Pila[];

    int contadorTipos=0;

    public int contextoID2;

    //Pilas de ejecucion

    Pila pilaOperadores;
    Pila pilaOperandos;
    Pila pilaTipos;
    Pila pilaAvail;
    Pila pilaSaltos;

    //Tabla de simbolos
    Object [][] tabla_simbolosE = new Object[999][5];
    int dir=0;
    /*Prueba*/
    public static ArrayList<Simbolo> tabla_simbolos = new ArrayList<Simbolo>();
    public static ArrayList<Simbolo> tabla_constantes = new ArrayList<Simbolo>();

    //Cuadruplos
    public static ArrayList<Cuadruplo> cuadruplos = new ArrayList<Cuadruplo>();

    int temporalForTf=0;
    String temporalForTfS = "";

    public Sintactico(){}


    //Metodo Analisis
    public void sintactico(JTextArea MainArea, Vector<String> InputVector, JTextArea StepsArea,String code){

        tablas = new Tablas();
        pilaOperadores = new Pila();
        pilaOperandos = new Pila();
        pilaTipos = new Pila();
        pilaAvail = new Pila();
        pilaSaltos = new Pila();
        contextoID2 = 0;
        for (int i =20; i>=1; i--){

            pilaAvail.push(500+i);
            pilaAvail.pushString("T"+String.valueOf(500+i));

        }
        for(int i = 0; i < InputVector.size(); i++){
            StepsArea.append(InputVector.get(i) + " \n");
        }

        InputVector.add(String.valueOf(943));
        int token;
        Pila pila = new Pila();
        pila.PUSH(943,StepsArea);
        MainArea.append("Valor Introducido a la PILA: " + 943 + "\n");
        pila.PUSH(0,StepsArea);
        MainArea.append("Valor Introducido a la PILA: " + 0 + "\n");
        boolean Flag = true;
        boolean dameOtro = true;

        token = Principal.dameToken(code.substring(0,code.length()-1));
        checaContexto(token);

        while(Flag)
        {

            if (token == 9999){
                token = Principal.dameToken(code.substring(0,code.length()-1));
                checaContexto(token);
            }

            int res=0;

            try {

                while (pila.LastValue()<900){

                    if (pila.lastValue() >=1000){
                        accion(pila.LastValue(),token);
                        MainArea.append("Valor Eliminado de la PILA: " + pila.POP(StepsArea) + "\n");
                    }

                    res = getValorTable(token,StepsArea,MainArea,pila);
                    System.out.println("Pila: "+String.valueOf(pila.LastValue())+" Token: "+String.valueOf(token)+ " Res: "+res);

                    if (pila.lastValue() >=1000){
                        accion(pila.LastValue(),token);
                        MainArea.append("Valor Eliminado de la PILA: " + pila.POP(StepsArea) + "\n");
                    }
                }

                while (pila.LastValue() >= 900){

                    if (pila.lastValue() >=1000){
                        accion(pila.LastValue(),token);
                        MainArea.append("Valor Eliminado de la PILA: " + pila.POP(StepsArea) + "\n");
                    }

                   if (pila.LastValue() == token){
                       MainArea.append("Valor Eliminado de la PILA: " + pila.POP(StepsArea) +" Porque encontro el token: "+token+ "\n");
                       if (pila.LastValue() >= 1000){
                           //es una accion
                           accion(pila.LastValue(),token);
                           MainArea.append("Valor Eliminado de la PILA: " + pila.POP(StepsArea) + "\n");

                       }
                       if (!(pila.lastValue() == 943)){
                           token = Principal.dameToken(code.substring(0,code.length()-1));
                           checaContexto(token);
                       }
                       //if (!(token == 902)){ token = Principal.dameToken(code.substring(0,code.length()-1)); checaContexto(token);}

                   } else {
                       if (pila.LastValue() >=900 && pila.LastValue() <1000){

                          //Hubo error sintactico porque es un valor terminal pero no el esperado
                           MainArea.append("Error de Sintaxis en su codigo fuente se esperaba: "+token+", se encontro: "+pila.LastValue());
                           JOptionPane.showMessageDialog(null,"Error, no se esperaba: "+Principal.variable,"Error de sintaxis", JOptionPane.ERROR_MESSAGE);
                           //Salir de los dos while hasta aqui llega el analisis
                           Principal.marcarError(Principal.variable);
                           Flag = false;
                           break;

                       } else {

                          if (res == 888){
                              MainArea.append("Error 888 de Sintaxis en su codigo fuente.");
                              JOptionPane.showMessageDialog(null,"Error","Error de sintaxis 2", JOptionPane.ERROR_MESSAGE);
                              Principal.marcarError(Principal.variable);
                              Flag = false;
                              break;
                          }
                       }
                   }


                   if (pila.LastValue() == 943) {
                       //Se compilo correctamente
                       MainArea.append("Valor borrado de la pila: " + pila.POP(StepsArea) +"\n");
                       JOptionPane.showMessageDialog(null, "Codigo correcto");
                       Flag=false;
                       break;

                   }



                }

            }   catch ( IndexOutOfBoundsException es){
                 MainArea.append("ERROR DE SINTAXIS EN SU CODIGO FUENTE");
                 JOptionPane.showMessageDialog(null, "Error desbordamiento","Error de sintaxis",JOptionPane.ERROR_MESSAGE);
                Principal.marcarError(Principal.variable);
                break;
            }


        }

    }  //Termina metodo sintactico

    public int getValorTable(int tokenEntrada, JTextArea StepsArea, JTextArea MainArea, Pila pila){


        int res = tablas.Tabla_Sintactico[pila.LastValue()][tokenEntrada-900];
        System.out.println("Pila: "+String.valueOf(pila.LastValue())+" Token: "+String.valueOf(tokenEntrada)+ " Res: "+res);
        MainArea.append("Valor Eliminado de la PILA: " + pila.POP(StepsArea) + "\n");
        for (int j = tablas.Tabla_Relacional[res - 1].length - 1; j >= 0; j--)
        {
            if (tablas.Tabla_Relacional[res - 1][j] == -1)
            {


            } else
            {
                MainArea.append("Valor Introducido a la PILA: " + tablas.Tabla_Relacional[res-1][j] + "\n");
                pila.PUSH(tablas.Tabla_Relacional[res-1][j],StepsArea);
            }


        }

        return res;

    }

    public void accion(int accion, int token){
        Tablas tabla = new Tablas();
        switch (accion){
            case 1001:
                if (contextoID2 == 1){
                    /*READ*/
                    Cuadruplo leer = new Cuadruplo();
                    Cuadruplo leerString = new Cuadruplo();
                    leer.generarCuadruplo(908,-1,-1,token);
                    leerString.generarCuadruploString("Input","","",Principal.variable);
                    cuadruplos.add(leer);
                    //JOptionPane.showMessageDialog(null,"Cuadruplo= Oper: "+"Input" + " res: "+leer.getRes());
                    Principal.modeloTablaCuadruplos.add(leerString);
                } else {
                    /*DECLARA*/
                    if (existeEnTablaSimnbolos(Principal.variable)){
                        JOptionPane.showMessageDialog(null, "La variable ( "+Principal.variable+" ) ya esta definida","Error",JOptionPane.ERROR_MESSAGE);

                    } else {
                        pilaOperandos.push(token);
                        pilaOperandos.pushString(Principal.variable);
                        contadorTipos++;
                    }
                }

                break;
            case 1002:
                pilaTipos.push(token);
                pilaTipos.pushString(Principal.variable);
                break;
            case 1003:
                    int  tipo = pilaTipos.pop();
                    String tipoString = pilaTipos.popString();
                    for (int i = 0; i<contadorTipos; i++){
                        Simbolo sim = new Simbolo(dir,pilaOperandos.pop(),pilaOperandos.popString(),tipo,tipoString,10000,10000);
                        tabla_simbolos.add(sim);
                        System.out.println("Dir: "+ sim.getId() +" Variable: "+sim.getVariable()+
                                " Var: "+sim.getDescrip() +" Tipo: "+sim.getTipo());
                        dir++;
                        Principal.modeloTablaSimbolos.add(sim);
                }
                contadorTipos=0;
                break;
            case 1011:
                pilaOperandos.push(token);
                pilaOperandos.pushString(Principal.variable);
                Principal.outputPilaOperandos.append(String.valueOf(token)+"\n");
                checaTablaSimnolos(Principal.variable,token);
                break;
            case 1012:
                pilaOperadores.push(token);
                pilaOperadores.pushString(Principal.variable);
                Principal.outputPilaOperadores.append(String.valueOf(token)+"\n");
                break;
            case 1013:
                pilaOperadores.push(token);
                pilaOperadores.pushString(Principal.variable);
                Principal.outputPilaOperadores.append(String.valueOf(token)+"\n");
                break;
            case 1014:
                /*Para Sumas y Restas*/
                if (!pilaOperadores.isEmpty()){
                    if (pilaOperadores.lastValue() == 932 || pilaOperadores.lastValue() == 933) {
                        Cuadruplo sumaresta = new Cuadruplo();
                        Cuadruplo sumarestaString = new Cuadruplo();
                        int operador = pilaOperadores.pop();
                        String operadors = pilaOperadores.popString();
                        int op2 = pilaOperandos.pop();
                        String op2s = pilaOperandos.popString();
                        int tipo1 = pilaTipos.pop();
                        pilaTipos.popString();
                        int op1 = pilaOperandos.pop();
                        String op1s = pilaOperandos.popString();
                        int tipo2 = pilaTipos.pop();
                        pilaTipos.popString();
                        if (renglonSemantico(tipo1,tipo2) == 9){
                            JOptionPane.showMessageDialog(null, "Tipos de datos incompatibles","Error",JOptionPane.ERROR_MESSAGE);
                            Principal.marcarError(Principal.variable);
                            break;
                        }
                        int tipoResultante = tabla.tabla_semantica[renglonSemantico(tipo1,tipo2)][columnaSemantica(operador)];
                        String tipoResultanteString = tipoString(tipoResultante);
                        int temp = pilaAvail.pop();
                        String temps = pilaAvail.popString();
                        sumaresta.generarCuadruplo(operador,op1,op2,temp);
                        sumarestaString.generarCuadruploString(operadors,op1s,op2s,"T"+String.valueOf(temp-500));
                        cuadruplos.add(sumaresta);
                        if(op1 >= 500 && op1 < 530){
                            pilaAvail.push(op1);
                            pilaAvail.pushString("T"+String.valueOf(op1));
                        }
                        if(op2 >= 500 && op2 < 530){
                            pilaAvail.push(op2);
                            pilaAvail.pushString("T"+String.valueOf(op1));
                        }
                        //JOptionPane.showMessageDialog(null,"Cuadruplo= Oper: "+sumaresta.getCodoper()+ " op1: "+ sumaresta.getOp1()
                        //+ " op2: "+sumaresta.getOp2()+ " res: "+sumaresta.getRes());
                        pilaOperandos.push(temp);
                        pilaOperandos.pushString("T"+String.valueOf(temp-500));
                        pilaTipos.push(tipoResultante);
                        pilaTipos.pushString(tipoResultanteString);
                        checaTablaSimnolos("T"+String.valueOf(temp-500),950);
                        actualizarTipoTemporal("T"+String.valueOf(temp-500),tipoResultante,tipoResultanteString);
                        Principal.outputPilaOperandos.append(String.valueOf(temp)+"\n");
                        Principal.modeloTablaCuadruplos.add(sumarestaString);
                    }
                }
                break;
            case 1015:
                if (!pilaOperadores.isEmpty()){
                    /* Multiplicacion y division*/
                    if (pilaOperadores.lastValue() == 934 || pilaOperadores.lastValue() == 935) {
                        Cuadruplo sumaresta = new Cuadruplo();
                        Cuadruplo sumarestaString = new Cuadruplo();
                        int operador = pilaOperadores.pop();
                        String operadors = pilaOperadores.popString();
                        int op2 = pilaOperandos.pop();
                        String op2s = pilaOperandos.popString();
                        int tipo1 = pilaTipos.pop();
                        pilaTipos.popString();
                        int op1 = pilaOperandos.pop();
                        String op1s = pilaOperandos.popString();
                        int tipo2 = pilaTipos.pop();
                        pilaTipos.popString();
                        if (renglonSemantico(tipo1,tipo2) == 9){
                            JOptionPane.showMessageDialog(null, "Tipos de datos incompatibles","Error",JOptionPane.ERROR_MESSAGE);
                            Principal.marcarError(Principal.variable);
                            break;
                        }
                        int tipoResultante = tabla.tabla_semantica[renglonSemantico(tipo1,tipo2)][columnaSemantica(operador)];
                        String tipoResultanteString = tipoString(tipoResultante);
                        int temp = pilaAvail.pop();
                        String temps = pilaAvail.popString();
                        sumaresta.generarCuadruplo(operador,op1,op2,temp);
                        sumarestaString.generarCuadruploString(operadors,op1s,op2s,"T"+String.valueOf(temp-500));
                        cuadruplos.add(sumaresta);
                        if(op1 >= 500 && op1 < 530){
                            pilaAvail.push(op1);
                            pilaAvail.pushString("T"+String.valueOf(op1));
                        }
                        if(op2 >= 500 && op2 < 530){
                            pilaAvail.push(op2);
                            pilaAvail.pushString("T"+String.valueOf(op1));
                        }
                        //JOptionPane.showMessageDialog(null,"Cuadruplo= Oper: "+sumaresta.getCodoper()+ " op1: "+ sumaresta.getOp1()
                        //        + " op2: "+sumaresta.getOp2()+ " res: "+sumaresta.getRes());
                        pilaOperandos.push(temp);
                        pilaOperandos.pushString("T"+String.valueOf(temp-500));
                        checaTablaSimnolos("T"+String.valueOf(temp-500),950);
                        actualizarTipoTemporal("T"+String.valueOf(temp-500),tipoResultante,tipoResultanteString);
                        Principal.outputPilaOperandos.append(String.valueOf(temp)+"\n");
                        Principal.modeloTablaCuadruplos.add(sumarestaString);
                    }

                }
                break;
            case 1016:
                pilaOperadores.push(-1);
                pilaOperadores.pushString("(");
                Principal.outputPilaOperadores.append(String.valueOf(-1)+"\n");
                break;
            case 1017:
                if (pilaOperadores.lastValue() == -1){
                    pilaOperadores.pop();
                    pilaOperadores.popString();
                }
                break;
            case 1018:
                pilaOperadores.push(token);
                pilaOperadores.pushString(Principal.variable);
                Principal.outputPilaOperadores.append(String.valueOf(token)+"\n");
                break;
            case 1019:
                /*Para operadores Relacionales*/
                if (pilaOperadores.lastValue() == 920 || pilaOperadores.lastValue() == 921 ||
                        pilaOperadores.lastValue() == 922 || pilaOperadores.lastValue() == 923 ||
                        pilaOperadores.lastValue() == 924 || pilaOperadores.lastValue() == 931) {
                    Cuadruplo opRel = new Cuadruplo();
                    Cuadruplo opRelString = new Cuadruplo();
                    int operador = pilaOperadores.pop();
                    String operadors = pilaOperadores.popString();
                    int op2 = pilaOperandos.pop();
                    String op2s = pilaOperandos.popString();
                    int tipo1 = pilaTipos.pop();
                    pilaTipos.popString();
                    int op1 = pilaOperandos.pop();
                    String op1s = pilaOperandos.popString();
                    int tipo2 = pilaTipos.pop();
                    pilaTipos.popString();
                    if (renglonSemantico(tipo1,tipo2) == 9){
                        JOptionPane.showMessageDialog(null, "Tipos de datos incompatibles","Error",JOptionPane.ERROR_MESSAGE);
                        Principal.marcarError(Principal.variable);
                        break;
                    }
                    int tipoResultante = tabla.tabla_semantica[renglonSemantico(tipo1,tipo2)][columnaSemantica(operador)];
                    String tipoResultanteString = tipoString(tipoResultante);
                    int temp = pilaAvail.pop();
                    String temps = pilaAvail.popString();
                    opRel.generarCuadruplo(operador,op1,op2,temp);
                    opRelString.generarCuadruploString(operadors,op1s,op2s,"T"+String.valueOf(temp-500));
                    cuadruplos.add(opRel);
                    if(op1 >= 500 && op1 < 530){
                        pilaAvail.push(op1);
                        pilaAvail.pushString("T"+String.valueOf(op1));
                    }
                    if(op2 >= 500 && op2 < 530){
                        pilaAvail.push(op2);
                        pilaAvail.pushString("T"+String.valueOf(op1));
                    }
                    //JOptionPane.showMessageDialog(null,"Cuadruplo= Oper: "+opRel.getCodoper()+ " op1: "+ opRel.getOp1()
                    //        + " op2: "+opRel.getOp2()+ " res: "+opRel.getRes());
                    pilaOperandos.push(temp);
                    pilaOperandos.pushString("T"+String.valueOf(temp-500));
                    checaTablaSimnolos("T"+String.valueOf(temp-500),950);
                    actualizarTipoTemporal("T"+String.valueOf(temp-500),tipoResultante,tipoResultanteString);
                    Principal.outputPilaOperandos.append(String.valueOf(temp)+"\n");
                    Principal.modeloTablaCuadruplos.add(opRelString);
                }
                break;
            case 1020:
                pilaOperadores.push(token);
                pilaOperadores.pushString(Principal.variable);
                Principal.outputPilaOperadores.append(String.valueOf(token)+"\n");
                break;
            case 1021:
                pilaOperadores.push(token);
                pilaOperadores.pushString(Principal.variable);
                Principal.outputPilaOperadores.append(String.valueOf(token)+"\n");
                break;
            case 1022:
                /*Para Operaciones OR*/
                if (!pilaOperadores.isEmpty()){
                    if (pilaOperadores.lastValue() == 917){
                        Cuadruplo or = new Cuadruplo();
                        Cuadruplo orString = new Cuadruplo();
                        int operador = pilaOperadores.pop();
                        String operadors = pilaOperadores.popString();
                        int op2 = pilaOperandos.pop();
                        String op2s = pilaOperandos.popString();
                        int tipo1 = pilaTipos.pop();
                        pilaTipos.popString();
                        int op1 = pilaOperandos.pop();
                        String op1s = pilaOperandos.popString();
                        int tipo2 = pilaTipos.pop();
                        pilaTipos.popString();
                        if (renglonSemantico(tipo1,tipo2) == 9){
                            JOptionPane.showMessageDialog(null, "Tipos de datos incompatibles","Error",JOptionPane.ERROR_MESSAGE);
                            Principal.marcarError(Principal.variable);
                            break;
                        }
                        int tipoResultante = tabla.tabla_semantica[renglonSemantico(tipo1,tipo2)][columnaSemantica(operador)];
                        String tipoResultanteString = tipoString(tipoResultante);
                        int temp = pilaAvail.pop();
                        String temps = pilaAvail.popString();
                        or.generarCuadruplo(operador,op1,op2,temp);
                        orString.generarCuadruploString(operadors,op1s,op2s,"T"+String.valueOf(temp-500));
                        cuadruplos.add(or);
                        if(op1 >= 500 && op1 < 530){
                            pilaAvail.push(op1);
                            pilaAvail.pushString("T"+String.valueOf(op1));
                        }
                        if(op2 >= 500 && op2 < 530){
                            pilaAvail.push(op2);
                            pilaAvail.pushString("T"+String.valueOf(op1));
                        }
                        //JOptionPane.showMessageDialog(null,"Cuadruplo= Oper: "+or.getCodoper()+ " op1: "+ or.getOp1()
                        //        + " op2: "+or.getOp2()+ " res: "+or.getRes());
                        pilaOperandos.push(temp);
                        pilaOperandos.pushString("T"+String.valueOf(temp-500));
                        checaTablaSimnolos("T"+String.valueOf(temp-500),950);
                        actualizarTipoTemporal("T"+String.valueOf(temp-500),tipoResultante,tipoResultanteString);
                        Principal.outputPilaOperandos.append(String.valueOf(temp)+"\n");
                        Principal.modeloTablaCuadruplos.add(orString);
                    }
                }

                break;
            //Testing
            case 1023:
                 /*Para Operaciones AND*/
                if (!pilaOperadores.isEmpty()){
                    if (pilaOperadores.lastValue() == 918){
                        Cuadruplo and = new Cuadruplo();
                        Cuadruplo andString = new Cuadruplo();
                        int operador = pilaOperadores.pop();
                        String operadors = pilaOperadores.popString();
                        int op2 = pilaOperandos.pop();
                        String op2s = pilaOperandos.popString();
                        int tipo1 = pilaTipos.pop();
                        pilaTipos.popString();
                        int op1 = pilaOperandos.pop();
                        String op1s = pilaOperandos.popString();
                        int tipo2 = pilaTipos.pop();
                        pilaTipos.popString();
                        if (renglonSemantico(tipo1,tipo2) == 9){
                            JOptionPane.showMessageDialog(null, "Tipos de datos incompatibles","Error",JOptionPane.ERROR_MESSAGE);
                            Principal.marcarError(Principal.variable);
                            break;
                        }
                        int tipoResultante = tabla.tabla_semantica[renglonSemantico(tipo1,tipo2)][columnaSemantica(operador)];
                        String tipoResultanteString = tipoString(tipoResultante);
                        int temp = pilaAvail.pop();
                        String temps = pilaAvail.popString();
                        and.generarCuadruplo(operador,op1,op2,temp);
                        andString.generarCuadruploString(operadors,op1s,op2s,"T"+String.valueOf(temp-500));
                        cuadruplos.add(and);
                        if(op1 >= 500 && op1 < 530){
                            pilaAvail.push(op1);
                            pilaAvail.pushString("T"+String.valueOf(op1));
                        }
                        if(op2 >= 500 && op2 < 530){
                            pilaAvail.push(op2);
                            pilaAvail.pushString("T"+String.valueOf(op1));
                        }
                        //JOptionPane.showMessageDialog(null,"Cuadruplo= Oper: "+and.getCodoper()+ " op1: "+ and.getOp1()
                        //        + " op2: "+and.getOp2()+ " res: "+and.getRes());
                        pilaOperandos.push(temp);
                        pilaOperandos.pushString("T"+String.valueOf(temp-500));
                        checaTablaSimnolos("T"+String.valueOf(temp-500),950);
                        actualizarTipoTemporal("T"+String.valueOf(temp-500),tipoResultante,tipoResultanteString);
                        Principal.outputPilaOperandos.append(String.valueOf(temp)+"\n");
                        Principal.modeloTablaCuadruplos.add(andString);
                    }
                }

                break;
            case 1024:
                pilaOperadores.push(token);
                pilaOperadores.pushString(Principal.variable);
                Principal.outputPilaOperadores.append(String.valueOf(token)+"\n");
                break;
            case 1025:
                    /*Para el NOT*/
                try {
                    if (!pilaOperadores.isEmpty() && pilaOperadores.lastValue() == 919){
                        //JOptionPane.showMessageDialog(null,"Vamo a negarlo");
                        Cuadruplo not = new Cuadruplo();
                        Cuadruplo notString = new Cuadruplo();
                        int operador = pilaOperadores.pop();
                        String operadors = pilaOperadores.popString();
                        int op1 = pilaOperandos.pop();
                        String op1s = pilaOperandos.popString();
                        int tipo1 = pilaTipos.pop();
                        pilaTipos.popString();
                        if (renglonSemantico(tipo1,907) == 9){
                            JOptionPane.showMessageDialog(null, "Tipos de datos incompatibles","Error",JOptionPane.ERROR_MESSAGE);
                            Principal.marcarError(Principal.variable);
                            break;
                        }
                        int tipoResultante = tabla.tabla_semantica[renglonSemantico(tipo1,907)][columnaSemantica(operador)];
                        String tipoResultanteString = tipoString(tipoResultante);
                        int temp = pilaAvail.pop();
                        String temps = pilaAvail.popString();
                        not.generarCuadruplo(operador,op1,-1,temp);
                        notString.generarCuadruploString(operadors,op1s,"","T"+String.valueOf(temp-500));
                        cuadruplos.add(not);
                        //JOptionPane.showMessageDialog(null,"Cuadruplo= Oper: "+not.getCodoper()+ " op1: "+ not.getOp1()
                        //        + " op2: "+not.getOp2()+ " res: "+not.getRes());
                        pilaOperandos.push(temp);
                        pilaOperandos.pushString("T"+String.valueOf(temp-500));
                        checaTablaSimnolos("T"+String.valueOf(temp-500),950);
                        actualizarTipoTemporal("T"+String.valueOf(temp-500),tipoResultante,tipoResultanteString);
                        Principal.outputPilaOperandos.append(String.valueOf(temp)+"\n");
                        Principal.modeloTablaCuadruplos.add(notString);
                    } else {

                    }
                }   catch (Exception es){

                }

                break;
            //Para Leer
            case 1028:
                /*Para READ*/
                Cuadruplo leer = new Cuadruplo();
                Cuadruplo leerString = new Cuadruplo();
                leer.generarCuadruplo(908,-1,-1,token);
                leerString.generarCuadruploString("Input","","",Principal.variable);
                cuadruplos.add(leer);
                //JOptionPane.showMessageDialog(null,"Cuadruplo= Oper: "+"Input" + " res: "+leer.getRes());
                Principal.modeloTablaCuadruplos.add(leerString);
                break;
            case 1029:
                /*Para ASIGNA*/
                pilaOperandos.push(token);
                pilaOperandos.pushString(Principal.variable);
                checaTablaSimnolos(Principal.variable,token);
                Principal.outputPilaOperandos.append(String.valueOf(token)+"\n");
                break;
            case 1030:
                Cuadruplo asignacion = new Cuadruplo();
                Cuadruplo asignacionString = new Cuadruplo();
                int var = pilaOperandos.pop();
                String vars = pilaOperandos.popString();
                int tipo1 = pilaTipos.pop();
                pilaTipos.popString();
                int resultadoExpresion = pilaOperandos.pop();
                String resultadoExpresions = pilaOperandos.popString();
                int tipo2 = pilaTipos.pop();
                pilaTipos.popString();
                if (renglonSemantico(tipo1,tipo2) == 9 ){
                    JOptionPane.showMessageDialog(null, "Tipos de datos incompatibles de asignacion","Error",JOptionPane.ERROR_MESSAGE);
                    Principal.marcarError(Principal.variable);
                    break;
                }

                int tipoResultante = tabla.tabla_semantica[renglonSemantico(tipo1,tipo2)][columnaSemantica(906)];
                if (tipoResultante == 0 ){
                    JOptionPane.showMessageDialog(null, "Tipos de datos incompatibles de asignacion","Error",JOptionPane.ERROR_MESSAGE);
                    Principal.marcarError(Principal.variable);
                    break;
                }
                asignacion.generarCuadruplo(926,var,-1,resultadoExpresion);
                asignacionString.generarCuadruploString("=",vars,"",resultadoExpresions);
                cuadruplos.add(asignacion);
                //JOptionPane.showMessageDialog(null,"Cuadruplo: Oper: "+" = " + " a variable: "+asignacion.getRes() + " de: "+ asignacion.getOp1());
                Principal.modeloTablaCuadruplos.add(asignacionString);
                break;
            case 1031:
                 /*Para WRITE todo falta pila tipos*/
                Cuadruplo write = new Cuadruplo();
                Cuadruplo writeString = new Cuadruplo();
                int resultadoExpresionAEscribir = pilaOperandos.pop();
                String resultadoExpresionAEscribirs = pilaOperandos.popString();
                pilaTipos.pop();
                pilaTipos.popString();
                write.generarCuadruplo(909,-1,-1,resultadoExpresionAEscribir);
                writeString.generarCuadruploString("Output","","",resultadoExpresionAEscribirs);
                cuadruplos.add(write);
                //JOptionPane.showMessageDialog(null,"Cuadruplo: Oper: "+"Output" + " res: "+write.getRes());
                Principal.modeloTablaCuadruplos.add(writeString);
                break;
            case 1032:
                /*Accion 1 de IF - ELSE*/
                if (pilaTipos.lastValueString().equals("BOOLEAN")){
                    int exp = pilaOperandos.pop() ;
                    String exps = pilaOperandos.popString();
                    pilaTipos.pop();
                    pilaTipos.popString();

                    Cuadruplo ifQuad = new Cuadruplo();
                    Cuadruplo ifQuadString = new Cuadruplo();
                    ifQuad.generarCuadruplo(2,exp,-1,0);
                    ifQuadString.generarCuadruploString("GOTOF",exps,"","?");
                    cuadruplos.add(ifQuad);
                    //JOptionPane.showMessageDialog(null,"Cuadruplo: "+"GOTOF" + " de: "+ifQuadString.getOp1S()+" a: ?");
                    Principal.modeloTablaCuadruplos.add(ifQuadString);

                    int contador = cuadruplos.size()+1;
                    pilaSaltos.push(contador-1);
                } else  JOptionPane.showMessageDialog(null, "La expresión del IF no es valida","Error Semántico",JOptionPane.ERROR_MESSAGE);
                break;
            case 1033:
                /*Accion 2 de IF - ELSE*/
                Cuadruplo elseQuad = new Cuadruplo();
                Cuadruplo elseQuadString = new Cuadruplo();
                elseQuad.generarCuadruplo(1,-1,-1,0);
                elseQuadString.generarCuadruploString("GOTO","","","?");
                cuadruplos.add(elseQuad);
                //JOptionPane.showMessageDialog(null,"Cuadruplo: Oper: "+"GOTO" + " a: "+elseQuadString.getResS());
                Principal.modeloTablaCuadruplos.add(elseQuadString);

                int dir = pilaSaltos.pop();
                int contadorif = cuadruplos.size()+1;
                rellena(dir,contadorif/*Segun contadorif+1*/);
                pilaSaltos.push(contadorif-1);
                //JOptionPane.showMessageDialog(null,"Rellena la direccion: "+dir + " con: "+(contadorif/*+1*/));

                break;

            case 1034:
                /*Accion 3 de IF - ELSE*/
                int direccion = pilaSaltos.pop();
                int contadorfin = cuadruplos.size()+1;
                rellena(direccion,contadorfin);

                //JOptionPane.showMessageDialog(null,"Rellena la direccion: "+direccion + " con: "+contadorfin);
                break;
            case 1035:
                /* Accion 1 de WHILE*/
                int contadorwhile = cuadruplos.size()+1;
                pilaSaltos.push(contadorwhile);
                break;
            case 1036:
                /* Accion 2 de WHILE*/
                if (pilaTipos.lastValueString().equals("BOOLEAN")){

                    int expwhile = pilaOperandos.pop() ;
                    String expwhiles = pilaOperandos.popString();
                    pilaTipos.pop();
                    pilaTipos.popString();

                    Cuadruplo whileQuad = new Cuadruplo();
                    Cuadruplo whileQuadString = new Cuadruplo();
                    whileQuad.generarCuadruplo(2,expwhile,-1,0);
                    whileQuadString.generarCuadruploString("GOTOF",expwhiles,"","?");
                    cuadruplos.add(whileQuad);
                    JOptionPane.showMessageDialog(null,"Cuadruplo: "+"GOTOF" + " de: "+whileQuadString.getOp1S()+" a: ?");
                    Principal.modeloTablaCuadruplos.add(whileQuadString);

                    int conta = cuadruplos.size()+1;
                    pilaSaltos.push(conta-1);

                } else  JOptionPane.showMessageDialog(null, "La expresión del WHILE no es valida","Error Semántico",JOptionPane.ERROR_MESSAGE);
                break;
            case 1037:
                /* Accion 3 de WHILE*/
                int falso = pilaSaltos.pop();
                int retorno = pilaSaltos.pop();

                Cuadruplo gotoretorno = new Cuadruplo();
                Cuadruplo gotoretornoString = new Cuadruplo();
                gotoretorno.generarCuadruplo(1,-1,-1,retorno);
                gotoretornoString.generarCuadruploString("GOTO","","",String.valueOf(retorno) );
                cuadruplos.add(gotoretorno);
                JOptionPane.showMessageDialog(null,"Cuadruplo: Oper: "+"GOTO" + " a: "+gotoretornoString.getResS());
                Principal.modeloTablaCuadruplos.add(gotoretornoString);

                int contadorfalso = cuadruplos.size()+1;
                rellena(falso,contadorfalso);
                JOptionPane.showMessageDialog(null,"Rellena la direccion: "+falso + " con: "+(contadorfalso));
                break;
            case 1038:
                /* Accion 1 de FOR*/
                pilaOperandos.push(token);
                pilaOperandos.pushString(Principal.variable);
                checaTablaSimnolos(Principal.variable,token);
                Principal.outputPilaOperandos.append(String.valueOf(token)+"\n");
                break;
            case 1039:
                /* Accion 2 de FOR*/
                int exp1 = pilaOperandos.pop();
                String exp1s = pilaOperandos.popString();
                pilaTipos.pop();
                pilaTipos.popString();
                int id = pilaOperandos.lastValue();
                String ids = pilaOperandos.lastValueString();

                Cuadruplo forQuad = new Cuadruplo();
                Cuadruplo forQuadString = new Cuadruplo();
                forQuad.generarCuadruplo(926,exp1,-1,id);
                forQuadString.generarCuadruploString("=",exp1s,"",ids);
                cuadruplos.add(forQuad);
                JOptionPane.showMessageDialog(null,"Cuadruplo: Oper: "+" = " + " a variable: "+forQuadString.getResS() + " de: "+ forQuadString.getOp1S());
                Principal.modeloTablaCuadruplos.add(forQuadString);
                break;
            case 1040:
                /* Accion 3 de FOR*/
                int Tf = pilaAvail.pop();
                temporalForTf = Tf;
                String Tfs = pilaAvail.popString();
                temporalForTfS = Tfs;
                int exp2 = pilaOperandos.pop();
                String exp2s = pilaOperandos.popString();
                pilaTipos.pop();
                pilaTipos.popString();
                //int Tx = pilaAvail.pop();
                //String Txs = pilaAvail.popString();

                //int idfor = pilaOperandos.lastValue();
                //String idfors = pilaOperandos.lastValueString();

                Cuadruplo asigna = new Cuadruplo();
                Cuadruplo asignaString = new Cuadruplo();

                //Cuadruplo menorigual = new Cuadruplo();
                //Cuadruplo menorigualString = new Cuadruplo();

                Cuadruplo gotof = new Cuadruplo();
                Cuadruplo gotofString = new Cuadruplo();

                asigna.generarCuadruplo(926,exp2,-1,Tf);
                asignaString.generarCuadruploString("=",exp2s,"","T"+String.valueOf(Tf-500));
                cuadruplos.add(asigna);
                Principal.modeloTablaCuadruplos.add(asignaString);

                Simbolo sim = new Simbolo(getDirGlobal(),950,"T"+String.valueOf(Tf-500),907,"BOOLEAN",10000,10000);
                tabla_constantes.add(sim);
                System.out.println("Dir: "+ sim.getId() +" Variable: "+sim.getVariable()+
                        " Var: "+sim.getDescrip() +" Tipo: "+sim.getTipo());
                sumarDir();
                Principal.modeloTablaConstantes.add(sim);
                /*menorigual.generarCuadruplo(922,idfor,Tf,Tx);
                menorigualString.generarCuadruploString("<=",idfors,"T"+String.valueOf(Tf-500),"T"+String.valueOf(Tx-500));
                cuadruplos.add(menorigual);
                Principal.modeloTablaCuadruplos.add(menorigualString);  */

                gotof.generarCuadruplo(2,Tf,-1,0);
                gotofString.generarCuadruploString("GOTOF","T"+String.valueOf(Tf-500),"","?");
                cuadruplos.add(gotof);
                Principal.modeloTablaCuadruplos.add(gotofString);

                //pilaAvail.push(Tx);
                //pilaAvail.pushString(Txs);

                int contafor = cuadruplos.size()+1;
                pilaSaltos.push(contafor-3);
                break;
            case 1041:
                /* Accion 4 de FOR*/
                int lastid = pilaOperandos.pop();
                String lastids = pilaOperandos.popString();
                pilaTipos.pop();
                pilaTipos.popString();

                Cuadruplo mas = new Cuadruplo();
                Cuadruplo mass = new Cuadruplo();

                int temp = pilaAvail.pop();
                String temps = pilaAvail.popString();

                mas.generarCuadruplo(932,lastid,1,temp);
                mass.generarCuadruploString("+",lastids,"1","T"+String.valueOf(temp-500));
                cuadruplos.add(mas);
                Principal.modeloTablaCuadruplos.add(mass);

                Simbolo sim2 = new Simbolo(getDirGlobal(),950,"T"+String.valueOf(temp-500),907,"BOOLEAN",10000,10000);
                tabla_constantes.add(sim2);
                System.out.println("Dir: "+ sim2.getId() +" Variable: "+sim2.getVariable()+
                        " Var: "+sim2.getDescrip() +" Tipo: "+sim2.getTipo());
                sumarDir();
                Principal.modeloTablaConstantes.add(sim2);

                Cuadruplo asignacion2 = new Cuadruplo();
                Cuadruplo asignacionString2 = new Cuadruplo();

                asignacion2.generarCuadruplo(926,temp,-1,lastid);
                asignacionString2.generarCuadruploString("=","T"+String.valueOf(temp-500),"",lastids);
                //checaTablaSimnolos("T"+String.valueOf(temp-500),950);
                cuadruplos.add(asignacion2);
                Principal.modeloTablaCuadruplos.add(asignacionString2);

                int retornofor = pilaSaltos.pop();
                Cuadruplo retornoFor = new Cuadruplo();
                Cuadruplo retornoForString = new Cuadruplo();

                retornoFor.generarCuadruplo(1,-1,-1,retornofor);
                retornoForString.generarCuadruploString("GOTO","","",String.valueOf(retornofor));

                cuadruplos.add(retornoFor);
                Principal.modeloTablaCuadruplos.add(retornoForString);

                int contafor2 = cuadruplos.size()+1;
                rellena(retornofor+2,contafor2);

                pilaAvail.push(temporalForTf);
                pilaAvail.pushString(temporalForTfS);
                break;
            default:
                JOptionPane.showMessageDialog(null,"Oye te falto programar la accion: "+accion);
                break;
        }
    }

    private void checaContexto(int token){
        /*Es un READ*/
        if (token == 908){
            contextoID2 = 1;
        }
            /*Es un DECLARA*/
        if (token == 903 || token == 904 || token == 905 || token == 906 || token == 907){
            contextoID2 = 2;
        }
    }

    private boolean existeEnTablaSimnbolos(String variable){
        for (Simbolo simbolo : tabla_simbolos){
             if (simbolo.getDescrip().equals(variable)){
                 return true;
             }
        }
        return false;
    }

    private boolean existeEnTablaConstantes(String variable){
        for (Simbolo simbolo : tabla_constantes){
            if (simbolo.getDescrip().equals(variable)){
                return true;
            }
        }
        return false;
    }

    public boolean sonTiposCompatibles(String tipo1, String tipo2){
        return true;
    }

    private void rellena(int dir, int posicion){

        int direccion = dir-1;
        cuadruplos.get(direccion).setRes(posicion);
        cuadruplos.get(direccion).setResS(String.valueOf(posicion));
        Principal.modeloTablaCuadruplos.setValueAt(posicion,direccion,3);

    }

    public void checaTablaSimnolos(String variable, int token){
        /*Si es un ID*/
        if (token == 930){
             if (existeEnTablaSimnbolos(variable)){
                 sacarTipoTablaSimbolos(variable);
             }  else
                 JOptionPane.showMessageDialog(null,"Error no se ha declarado la variable ( "+ variable+") ","Error de declaración",JOptionPane.ERROR_MESSAGE);
          }
        /*Si es un temporal*/
        if (token == 950){
            if (existeEnTablaConstantes(variable)){
              sacarTipoTablaConstantes(variable);
            } else meterEnTablaSimbolos(token,variable,951);
        }
        /*Si es otra cosa revisar si existen si no existe meterlo en la tabla y sacar tipo
        * si ya existe actualizar si es que se actualiza y sacar tipo nuevo o anterior*/
        switch (token){
            case 936:
                if (existeEnTablaConstantes(variable)){
                    pilaTipos.push(903);
                    pilaTipos.pushString(tipoString(903));
                    Principal.outputPilaTipo.append(String.valueOf(903)+"\n");
                } else {
                   meterEnTablaSimbolos(token,variable,903);
                }
                break;
            case 937:
                if (existeEnTablaConstantes(variable)){
                    pilaTipos.push(904);
                    pilaTipos.pushString(tipoString(904));
                    Principal.outputPilaTipo.append(String.valueOf(904)+"\n");
                } else {
                    meterEnTablaSimbolos(token,variable,904);
                }
                break;
            case 938:
                if (existeEnTablaConstantes(variable)){
                    pilaTipos.push(904);
                    pilaTipos.pushString(tipoString(904));
                    Principal.outputPilaTipo.append(String.valueOf(904)+"\n");
                } else {
                    meterEnTablaSimbolos(token,variable,904);
                }
                break;
            case 939:
                if (existeEnTablaConstantes(variable)){
                    pilaTipos.push(905);
                    pilaTipos.pushString(tipoString(905));
                    Principal.outputPilaTipo.append(String.valueOf(905)+"\n");
                } else {
                    meterEnTablaSimbolos(token,variable,905);
                }
                break;
            case 940:
                if (existeEnTablaConstantes(variable)){
                    pilaTipos.push(906);
                    pilaTipos.pushString(tipoString(906));
                    Principal.outputPilaTipo.append(String.valueOf(906)+"\n");
                } else {
                    meterEnTablaSimbolos(token,variable,906);
                }
                break;
            case 941:
                if (existeEnTablaConstantes(variable)){
                    pilaTipos.push(907);
                    pilaTipos.pushString(tipoString(907));
                    Principal.outputPilaTipo.append(String.valueOf(907)+"\n");
                } else {
                    meterEnTablaSimbolos(token,variable,907);
                }
                break;
            case 942:
                if (existeEnTablaConstantes(variable)){
                    pilaTipos.push(907);
                    pilaTipos.pushString(tipoString(907));
                    Principal.outputPilaTipo.append(String.valueOf(907)+"\n");
                } else {
                    meterEnTablaSimbolos(token,variable,907);
                }
                break;
        }
    }

    public void meterEnTablaSimbolos(int token, String descrip, int tipo){
        String tipoString = tipoString(tipo);
        Simbolo sim = new Simbolo(dir,token,descrip,tipo,tipoString,10000,10000);
        tabla_constantes.add(sim);
        System.out.println("Dir: "+ sim.getId() +" Variable: "+sim.getVariable()+
                " Var: "+sim.getDescrip() +" Tipo: "+sim.getTipo());
        dir++;
        Principal.modeloTablaConstantes.add(sim);
        pilaTipos.push(tipo);
        pilaTipos.pushString(tipoString);
        Principal.outputPilaTipo.append(String.valueOf(tipo)+"\n");
    }

    public String sacarTipoTablaSimbolos(String variable){
        for (Simbolo s : tabla_simbolos){
            if (s.getDescrip().equals(variable)){
                pilaTipos.push(s.getTipo());
                pilaTipos.pushString(tipoString(s.getTipo()));
                Principal.outputPilaTipo.append(String.valueOf(s.getTipo())+"\n");
                return tipoString(s.getTipo());
            }
        }
       return null;
    }

    public void actualizarTipoTemporal(String id,int tipo, String tipoString){
        for (Simbolo s : tabla_constantes){
            if (s.getDescrip().equals(id)){
                s.setTipo(tipo);
                s.setTipoS(tipoString);
                pilaTipos.pop();
                pilaTipos.popString();
                pilaTipos.push(s.getTipo());
                pilaTipos.pushString(tipoString(s.getTipo()));
            }
        }

        for (int i=0; i < Principal.modeloTablaConstantes.regresa().size(); i++){
            Simbolo s = Principal.modeloTablaConstantes.get(i);
            if (s.getDescrip().equals(id)){
                Principal.modeloTablaConstantes.setValueAt(tipoString,i,3);
            }
        }
        for (Simbolo s : Principal.modeloTablaConstantes.regresa()){

        }
    }

    public String sacarTipoTablaConstantes(String variable){
        for (Simbolo s : tabla_constantes){
            if (s.getDescrip().equals(variable)){
                pilaTipos.push(s.getTipo());
                pilaTipos.pushString(tipoString(s.getTipo()));
                Principal.outputPilaTipo.append(String.valueOf(s.getTipo())+"\n");
                return tipoString(s.getTipo());
            }
        }
        return null;
    }
    public String tipoString(int tipo){
         switch (tipo) {
             case 903:
                 return "INTEGER";
             case 904:
                 return "FLOAT";
             case 905:
                 return "CHAR";
             case 906:
                 return "STRING";
             case 907:
                 return "BOOLEAN";
             case 951:
                 return "TEMPORAL";
         }
        return null;
    }

    private void sumarDir(){
        dir++;
    }
    private int getDirGlobal(){
       return dir;
    }


    //Metodos para revisar semantica

    public int renglonSemantico(int tipo1, int tipo2){
        if (tipo1 == 903 && tipo2 == 903){
          return 0;
        }
        if (tipo1 == 903 && tipo2 == 904){
            return 1;
        }
        if (tipo1 == 904 && tipo2 == 903){
            return 2;
        }
        if (tipo1 == 904 && tipo2 == 904){
            return 3;
        }
        if (tipo1 == 905 && tipo2 == 905){
            return 4;
        }
        if (tipo1 == 905 && tipo2 == 906){
            return 5;
        }
        if (tipo1 == 906 && tipo2 == 905){
            return 6;
        }
        if (tipo1 == 906 && tipo2 == 906){
            return 7;
        }
        if (tipo1 == 907 && tipo2 == 907){
            return 8;
        }
        return 9;
    }

    public int columnaSemantica(int operador){
         switch (operador){
             /*| + | - | * | */
             case 932: return 0;
             case 933: return 0;
             case 934: return 0;
             /*| / | */
             case 935: return 1;
             /*| div | mod | */

             /*| == | != | < | <= | > | >= | */
             case 931: return 3;
             case 920: return 3;
             case 921: return 3;
             case 922: return 3;
             case 923: return 3;
             case 924: return 3;
             /*| AND | OR |  | */
             case 917: return 4;
             case 918: return 4;
             case 926: return 5;
         }
        return 5;
    }

}  //Se termina clase