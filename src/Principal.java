import jdk.nashorn.internal.scripts.JO;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Principal implements Serializable  {

    static Tablas tablas;
    Sintactico sintactico;
    static Vector<String> inputStream;
    JTextArea outputAreaSint;
    JTextArea outputAreaESTADOS;
    static JTextArea outputArea;
    static JTextArea inputArea;
    static JTextArea consoleArea;
    static int puntero=0;

    public static JTextArea outputPilaOperadores;
    public static JTextArea outputPilaOperandos;
    public static JTextArea outputPilaTipo;
    public static JTextArea outputPilaAvail;
    public static JTextArea outputPilaSaltos;

    public static JTable tablaSimbolos;
    public static ModelTableSimbolos modeloTablaSimbolos;
    public static JTable tablaCuadruplos;
    public static ModelTableCuadruplos modeloTablaCuadruplos;
    public static JTable tablaConstantes;
    public static ModelTableConstantes modeloTablaConstantes;

    //Token encontrado en forma de texto (NO numerico)
    static String variable;

    //
    String nombreArchivo="Programa";
    String codigoFuente = "";


    //Consola de comandos punteros
    private static int punteroAnterior;
    private static int punteroActual;
    //Programa cargado de disco
    public static H programa;
    //Hilo de Ejecucion
    private static Thread hiloEjecucion;
    private static boolean pausado=false;
    private static boolean detenido=false;
    private static boolean termino=false;
    private static String variableLeida;
    private static int ic=0;

    ArrayList<SimboloEjecucion> tabla_ejecucion = new ArrayList<SimboloEjecucion>();

    public void Frame(){
        programa=null;
        sintactico = new Sintactico();
        //Ventana
        final JFrame jFrame = new JFrame("Analizador");jFrame.setSize(1300,650);jFrame.setLocationRelativeTo(null);
        jFrame.setResizable(false);jFrame.setDefaultCloseOperation(jFrame.EXIT_ON_CLOSE);
        jFrame.setLayout(null);
        ImageIcon raiz = new ImageIcon("img\\fondo.png");
        ImageIcon iconf = new ImageIcon(raiz.getImage().getScaledInstance(jFrame.getWidth(), jFrame.getHeight(), java.awt.Image.SCALE_SMOOTH));
        JLabel labelfondo = new JLabel();
        labelfondo.setSize(jFrame.getWidth(), jFrame.getHeight());
        labelfondo.setIcon(iconf);
        Cursor C = new Cursor(Cursor.HAND_CURSOR);

        Font f = new Font("Arial",Font.ITALIC,20);
        //Font f2 = new Font("Arial",Font.ITALIC,16);

        JLabel cFuente = new JLabel("Codigo Fuente"); cFuente.setBounds(80,10,200,50); cFuente.setFont(f);
        cFuente.setForeground(new Color(151, 173, 193));

        JLabel cAnalizado = new JLabel("Analizador Lexico"); cAnalizado.setBounds(470,10,200,50); cAnalizado.setFont(f);
        cAnalizado.setForeground(new Color(151, 173, 193));

        JLabel cAnalizadoSint = new JLabel("Analizador Sintactico"); cAnalizadoSint.setBounds(75,360,200,50); cAnalizadoSint.setFont(f);
        cAnalizadoSint.setForeground(new Color(151, 173, 193));

        JLabel procPila = new JLabel("Proceso de Pila"); procPila.setBounds(420,360,200,50); procPila.setFont(f);
        procPila.setForeground(new Color(151, 173, 193));

        JLabel procOper = new JLabel("Operador"); procOper.setBounds(695,10,200,50); procOper.setFont(f);
        procOper.setForeground(new Color(151, 173, 193));

        JLabel procOperando = new JLabel("Operando"); procOperando.setBounds(790,10,200,50); procOperando.setFont(f);
        procOperando.setForeground(new Color(151, 173, 193));

        JLabel procTipo = new JLabel("Tipo"); procTipo.setBounds(900,10,200,50); procTipo.setFont(f);
        procTipo.setForeground(new Color(151, 173, 193));

        JLabel procTablaSimbolos = new JLabel("Tabla de Símbolos"); procTablaSimbolos.setBounds(725,360,200,50); procTablaSimbolos.setFont(f);
        procTablaSimbolos.setForeground(new Color(151, 173, 193));

        JLabel procTablaCuadruplos = new JLabel("Tabla de Cuádruplos"); procTablaCuadruplos.setBounds(1040,10,200,50); procTablaCuadruplos.setFont(f);
        procTablaCuadruplos.setForeground(new Color(151, 173, 193));

        JLabel procTablaConstantes = new JLabel("Tabla de Constantes"); procTablaConstantes.setBounds(1040,360,200,50); procTablaConstantes.setFont(f);
        procTablaConstantes.setForeground(new Color(151, 173, 193));

        //Text Areas
        //textarea1
        inputArea = new JTextArea(); inputArea.setLineWrap(true);
        JScrollPane inputScrollPane = new JScrollPane(inputArea);
        inputScrollPane.setBounds(20,50,250,300);
        inputArea.addKeyListener(new KeyListener() {

            @Override
            public void keyTyped(KeyEvent event) {

                char caracter = event.getKeyChar();
                if (caracter == KeyEvent.VK_ENTER) {
                    FileWriter fw = null;
                    BufferedWriter bw = null;
                    try {
                        fw = new FileWriter("read.txt");
                        bw = new BufferedWriter(fw);
                        bw.write("\n");

                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            if (null != fw)
                                fw.close();
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                    }

                }

            }

            @Override
            public void keyPressed(KeyEvent ke) {
            }

            @Override
            public void keyReleased(KeyEvent ke) {
            }
        });

        //textarea2
        outputArea = new JTextArea(); outputArea.setLineWrap(true);
        JScrollPane outputScrollPane = new JScrollPane(outputArea);
        outputScrollPane.setBounds(428, 50, 250, 300); outputArea.setEditable(false);

        int tamx = 300;
        //textarea3
        outputAreaSint = new JTextArea(); outputAreaSint.setLineWrap(true);
        JScrollPane outputScrollPaneSint = new JScrollPane(outputAreaSint);
        outputScrollPaneSint.setBounds(20,400,tamx,200); outputAreaSint.setEditable(false);

        //textarea4
        outputAreaESTADOS = new JTextArea(); outputAreaESTADOS.setLineWrap(true);
        JScrollPane outputScrollPaneESTADOS = new JScrollPane(outputAreaESTADOS);
        outputScrollPaneESTADOS.setBounds(340,400,tamx,200); outputAreaESTADOS.setEditable(false);

        //textareaOperadores
        outputPilaOperadores = new JTextArea(); outputPilaOperadores.setLineWrap(true);
        outputPilaOperadores.setFont(f);
        JScrollPane outputScrollPaneOperadores = new JScrollPane(outputPilaOperadores);
        outputScrollPaneOperadores.setBounds(705,50,70,300); outputPilaOperadores.setEditable(true);

        //textareaOperandos
        outputPilaOperandos = new JTextArea(); outputPilaOperandos.setLineWrap(true);
        outputPilaOperandos.setFont(f);
        JScrollPane outputScrollPaneOperandos = new JScrollPane(outputPilaOperandos);
        outputScrollPaneOperandos.setBounds(795,50,70,300); outputPilaOperandos.setEditable(true);

        //textareaTipo
        outputPilaTipo = new JTextArea(); outputPilaTipo.setLineWrap(true);
        outputPilaTipo.setFont(f);
        JScrollPane outputScrollPaneTipo = new JScrollPane(outputPilaTipo);
        outputScrollPaneTipo.setBounds(885,50,70,300); outputPilaTipo.setEditable(true);

        //Botones
        //Abrir
        ImageIcon dirAbrir = new ImageIcon("img\\abrir.png");
        ImageIcon confAbrir = new ImageIcon(dirAbrir.getImage().getScaledInstance(75, 75, java.awt.Image.SCALE_SMOOTH));
        JLabel labelAbrir = new JLabel();
        labelAbrir.setIcon(confAbrir);
        labelAbrir.setCursor(C);
        labelAbrir.setBounds(310,40,75,75);
        labelAbrir.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {
                Read(inputArea, jFrame);
            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        //Compilar
        ImageIcon dirAnalizar = new ImageIcon("img\\analizar.png");
        ImageIcon confAnalizar = new ImageIcon(dirAnalizar.getImage().getScaledInstance(75, 75, java.awt.Image.SCALE_SMOOTH));
        JLabel labelAnalizar = new JLabel();
        labelAnalizar.setIcon(confAnalizar);
        labelAnalizar.setCursor(C);
        labelAnalizar.setBounds(310,120,75,75);
        labelAnalizar.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {
                outputArea.setText(null);
                outputAreaSint.setText(null);
                outputAreaESTADOS.setText(null);
                outputPilaOperadores.setText(null);
                outputPilaOperandos.setText(null);
                outputPilaTipo.setText(null);
                Sintactico.tabla_constantes = new ArrayList<Simbolo>();;
                Sintactico.tabla_simbolos = new ArrayList<Simbolo>();;
                Sintactico.cuadruplos = new ArrayList<Cuadruplo>();;
                sintactico = null;
                programa=null;
                sintactico = new Sintactico();
                modeloTablaCuadruplos.clean();
                modeloTablaSimbolos.clean();
                modeloTablaConstantes.clean();

                inputStream = new Vector<String>();
                Unpack(inputArea);
                puntero=0;
                //Compilar(outputArea);
                try {
                    //System.out.println(dameCodigo());
                    codigoFuente = dameCodigo();
                    sintactico.sintactico(outputAreaSint, inputStream, outputAreaESTADOS,codigoFuente);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        //Limpiar
        ImageIcon dirLimpiar = new ImageIcon("img\\limpiar.png");
        ImageIcon cofLimpiar = new ImageIcon(dirLimpiar.getImage().getScaledInstance(75, 75, java.awt.Image.SCALE_SMOOTH));
        JLabel labelLimpiar = new JLabel();
        labelLimpiar.setIcon(cofLimpiar);
        labelLimpiar.setCursor(C);
        labelLimpiar.setBounds(310, 200, 75, 75);
        labelLimpiar.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {
                inputArea.setText(null);
                outputArea.setText(null);
                outputAreaSint.setText(null);
                outputAreaESTADOS.setText(null);
                outputPilaOperadores.setText(null);
                outputPilaOperandos.setText(null);
                outputPilaTipo.setText(null);
                Sintactico.tabla_constantes = new ArrayList<Simbolo>();;
                Sintactico.tabla_simbolos = new ArrayList<Simbolo>();;
                Sintactico.cuadruplos = new ArrayList<Cuadruplo>();;
                sintactico = null;
                programa=null;
                sintactico = new Sintactico();
                modeloTablaCuadruplos.clean();
                modeloTablaSimbolos.clean();
                modeloTablaConstantes.clean();
                /*outputPilaAvail.setText(null);
                outputPilaSaltos.setText(null); */
            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        //Boton guardar programa compilado
        final JButton guardar = new JButton("Guardar");
        guardar.setCursor(C);
        guardar.setBounds(310,290,80,40);
        guardar.setOpaque(false);
        guardar.setContentAreaFilled(false);
        guardar.setBorderPainted(false);
        guardar.setForeground(Color.white);
        guardar.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {

               nombreArchivo = String.valueOf(JOptionPane.showInputDialog(null,"Ingresa el nombre del archivo"));
                H programa = null;
                try {
                    programa = new H(nombreArchivo);
                } catch (IOException es) {
                    es.printStackTrace();
                } catch (ClassNotFoundException es) {
                    es.printStackTrace();
                }
                if (!nombreArchivo.equals("")){
                    if (programa!= null && programa.addH(Sintactico.cuadruplos,Sintactico.tabla_simbolos,Sintactico.tabla_constantes,codigoFuente)) {

                    } else
                        JOptionPane.showMessageDialog(null, "Hubo un problema al guardar el programa","Error de guardado",JOptionPane.ERROR_MESSAGE);

                }  else
                    JOptionPane.showMessageDialog(null, "Hubo un problema al guardar el programa","Error de guardado",JOptionPane.ERROR_MESSAGE);
            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                guardar.setOpaque(true);
                guardar.setContentAreaFilled(true);
                guardar.setBackground(new Color(7, 36, 41));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                guardar.setOpaque(false);
                guardar.setContentAreaFilled(false);
                guardar.setBorderPainted(false);
            }
        });

        //Boton guardar programa compilado
        final JButton cargar = new JButton("Cargar");
        cargar.setCursor(C);
        cargar.setBounds(310,340,80,40);
        cargar.setOpaque(false);
        cargar.setContentAreaFilled(false);
        cargar.setBorderPainted(false);
        cargar.setForeground(Color.white);
        cargar.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {
                ReadProgram(inputArea, jFrame);

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                cargar.setOpaque(true);
                cargar.setContentAreaFilled(true);
                cargar.setBackground(new Color(7, 36, 41));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                cargar.setOpaque(false);
                cargar.setContentAreaFilled(false);
                cargar.setBorderPainted(false);
            }
        });

        //***************** T A B L A S *****************

        modeloTablaSimbolos = new ModelTableSimbolos();

        tablaSimbolos = new JTable(modeloTablaSimbolos);
        tablaSimbolos.setSize(tamx, 200);
        tablaSimbolos.setBackground(Color.WHITE);

        JScrollPane scrollSimbolos = new JScrollPane(tablaSimbolos);
        scrollSimbolos.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollSimbolos.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollSimbolos.setBounds(660, 400, tablaSimbolos.getWidth(), tablaSimbolos.getHeight());

        modeloTablaCuadruplos = new ModelTableCuadruplos();

        tablaCuadruplos = new JTable(modeloTablaCuadruplos);
        tablaCuadruplos.setSize(tamx, 300);
        tablaCuadruplos.setBackground(Color.WHITE);

        JScrollPane scrollCuadruplos = new JScrollPane(tablaCuadruplos);
        scrollCuadruplos.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollCuadruplos.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollCuadruplos.setBounds(980, 50, tablaCuadruplos.getWidth(), tablaCuadruplos.getHeight());

        modeloTablaConstantes = new ModelTableConstantes();

        tablaConstantes = new JTable(modeloTablaConstantes);
        tablaConstantes.setSize(tamx, 200);
        tablaConstantes.setBackground(Color.WHITE);

        JScrollPane scrollConstantes = new JScrollPane(tablaConstantes);
        scrollConstantes.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollConstantes.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollConstantes.setBounds(980, 400, tablaConstantes.getWidth(), tablaConstantes.getHeight());


        //Frame
        jFrame.add(procPila);
        jFrame.add(procOper);
        jFrame.add(procOperando);
        jFrame.add(procTipo);
        jFrame.add(procTablaCuadruplos);
        jFrame.add(procTablaSimbolos);
        jFrame.add(procTablaConstantes);
        jFrame.add(outputScrollPaneESTADOS);
        jFrame.add(cAnalizadoSint);
        jFrame.add(cAnalizado);
        jFrame.add(cFuente);
        jFrame.add(labelAbrir);
        jFrame.add(labelAnalizar);
        jFrame.add(labelLimpiar);
        jFrame.add(guardar);
        jFrame.add(cargar);
        jFrame.add(outputScrollPane);
        jFrame.add(inputScrollPane);
        jFrame.add(outputScrollPaneSint);
        jFrame.add(outputScrollPaneOperadores);
        jFrame.add(outputScrollPaneOperandos);
        jFrame.add(outputScrollPaneTipo);
        jFrame.add(scrollSimbolos);
        jFrame.add(scrollCuadruplos);
        jFrame.add(scrollConstantes);
        jFrame.add(labelfondo);


        jFrame.setVisible(true);
    }

    public void consolaFrame(){
        punteroAnterior = 0;
        punteroActual = 0;
        hiloEjecucion = null;
        pausado=false;
        detenido=false;
        termino=false;
        variableLeida = "";
        //Ventana
        final JFrame jFrame = new JFrame("H Console");jFrame.setSize(600,400);jFrame.setLocationRelativeTo(null);
        jFrame.setResizable(false);jFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        jFrame.setBackground(Color.black);
        jFrame.setLayout(null);

        jFrame.getContentPane().setBackground(Color.black);

        Font consoleFont = new Font("Consolas",Font.PLAIN,16);
        //textarea
        consoleArea = new JTextArea();
        consoleArea.setWrapStyleWord(true);
        consoleArea.setBackground(Color.black);
        consoleArea.setForeground(Color.green);
        consoleArea.setFont(consoleFont);
        consoleArea.setCaretColor(Color.green);
        consoleArea.append("H console [versión 1.0.1 release 428]\n\n>");
        consoleArea.setCaretPosition(consoleArea.getText().length());
        JScrollPane inputScrollPane = new JScrollPane(consoleArea);
        inputScrollPane.setBounds(1,0,jFrame.getWidth()-5,jFrame.getHeight()-25);
        punteroAnterior = consoleArea.getText().length();
        consoleArea.addKeyListener(new KeyListener() {

            @Override
            public void keyTyped(KeyEvent event) {

                char caracter = event.getKeyChar();
                punteroActual = consoleArea.getText().length();
                if (caracter == KeyEvent.VK_ENTER) {
                    if (hiloEjecucion.isAlive()){
                        hiloEjecucion.resume();
                        detenido=false;
                    }
                    else{
                        if (termino){
                           JOptionPane.showMessageDialog(null,"Ya termino la ejecucion");
                        } else
                        {
                            hiloEjecucion.start();
                            detenido=false;
                        }

                    }
                    if (pausado) {
                        pausado=false;
                        hiloEjecucion.resume();
                    }
                    //JOptionPane.showMessageDialog(null,"\""+consoleArea.getText().substring(punteroAnterior,punteroActual-1)+"\"");
                    variableLeida = consoleArea.getText().substring(punteroAnterior,punteroActual-1);
                    if (programa.getTabla_simbolos()!=null){
                        for (Simbolo s : programa.getTabla_simbolos()){
                             if (s.getDescrip().equals(programa.cuadruplosTabla.get(ic).getResS())){
                                 s.setValor(variableLeida);
                             }
                        }
                    }
                    consoleArea.append(">");
                    punteroAnterior = consoleArea.getText().length();
                }

            }

            @Override
            public void keyPressed(KeyEvent ke) {
            }

            @Override
            public void keyReleased(KeyEvent ke) {
            }
        });


        jFrame.add(inputScrollPane);


        jFrame.setVisible(true);
    }

     /*TODO:
     * *** Semantica **
     * *** Liberar Temporales **
     * *** Arreglar forma de pedir tokens para arreglar dos parentesis seguidos
     * *** lo del AND
     * */

    public void Read(JTextArea jTextArea, JFrame jFrame) {
        JFileChooser jFileChooser = new JFileChooser();
        jFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        FileNameExtensionFilter extensionFilter = new FileNameExtensionFilter("txt","txt");
        jFileChooser.setFileFilter(extensionFilter);

        try
        {
            int Open = jFileChooser.showDialog(jFrame,"Abrir");
            String path = jFileChooser.getSelectedFile().getParent() + "/" + jFileChooser.getSelectedFile().getName();
            if(Open == JFileChooser.APPROVE_OPTION)
            {
                try{

                    BufferedReader leer = new BufferedReader(new InputStreamReader(new FileInputStream(path), "ISO-8859-1"));
                    String linea = "";
                    String contenido = "";

                    while((linea = leer.readLine()) != null )
                    {
                        contenido += linea +"\n" ;
                    }
                    contenido = contenido.substring(0, contenido.length()-1);
                    leer.close();
                    jTextArea.setText(contenido);

                } catch (Exception e) {}
            }
        }
        catch (Exception e) {}
    }

    public void ReadProgram(JTextArea jTextArea, JFrame jFrame){
        JFileChooser jFileChooser = new JFileChooser();
        jFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        FileNameExtensionFilter extensionFilter = new FileNameExtensionFilter(".h","h");
        jFileChooser.setFileFilter(extensionFilter);

        try
        {
            int Open = jFileChooser.showDialog(jFrame,"Abrir");
            String path = jFileChooser.getSelectedFile().getParent() + "/" + jFileChooser.getSelectedFile().getName();
            JOptionPane.showMessageDialog(null,path);

            if(Open == JFileChooser.APPROVE_OPTION)
            {
                try{
                    programa = null;
                    try {
                        String file = jFileChooser.getSelectedFile().getName();
                        programa = new H(file.substring(0,file.length()-2));
                        programa = programa.recuperar("hpp");
                    } catch (IOException es) {
                        es.printStackTrace();
                    } catch (ClassNotFoundException es) {
                        es.printStackTrace();
                    }
                    inputArea.setText(null);
                    outputArea.setText(null);
                    outputAreaSint.setText(null);
                    outputAreaESTADOS.setText(null);
                    outputPilaOperadores.setText(null);
                    outputPilaOperandos.setText(null);
                    outputPilaTipo.setText(null);
                    Sintactico.tabla_constantes = new ArrayList<Simbolo>();;
                    Sintactico.tabla_simbolos = new ArrayList<Simbolo>();;
                    Sintactico.cuadruplos = new ArrayList<Cuadruplo>();;
                    sintactico = null;
                    sintactico = new Sintactico();
                    modeloTablaCuadruplos.clean();
                    modeloTablaSimbolos.clean();
                    modeloTablaConstantes.clean();

                    jTextArea.setText(programa.getCodigoFuente());
                    for (Simbolo s : programa.getSimbolosTabla()){
                         Principal.modeloTablaSimbolos.add(s);
                    }
                    for (Simbolo s : programa.getConstantesTabla()){
                        Principal.modeloTablaConstantes.add(s);
                    }
                    for (Cuadruplo c : programa.getCuadruplosTabla()){
                        Principal.modeloTablaCuadruplos.add(c);
                    }

                    consolaFrame();
                    ejecuta();

                } catch (Exception e) {e.printStackTrace();}
            }
        }
        catch (Exception e) {
            e.printStackTrace();;
        }
    }

    public static int dameToken(String word){
       // System.out.println(word);
        tablas = new Tablas();
        int terminal=0;
        int punteroAnterior = puntero-1;
        int estado=0;
        int col;
        String analiza="";

        while (estado<=18) {
            int character = word.charAt(puntero);
            analiza += (char) character;
            col = Link((char)character);
            //estado = tablas.Tabla_lexico[estado][col];
            estado = tablas.tablaLexico(estado,col);
            puntero++;
            punteroAnterior++;

            if (puntero == word.length() && estado <= 18){
                //estado = tablas.Tabla_lexico[estado][27];
                estado = tablas.tablaLexico(estado,27);
            }
        }


        if(estado == 100) {
            if (puntero == word.length() && analiza.charAt(analiza.length() - 1) >= 'A' && analiza.charAt(analiza.length() - 1) <= 'Z') {
                analiza += " ";
            } else {
                puntero = punteroAnterior;
            }
            String term = analiza.substring(0, analiza.length() - 1);
            if (term.trim().equals("CLASS")) {
                terminal = 900;
            }
            if (term.trim().equals("BEGIN")) {
                terminal = 901;
            }
            if (term.trim().equals("ENDCLASS")) {
                terminal = 902;
            }
            if (term.trim().equals("INTEGER")) {
                terminal = 903;
            }
            if (term.trim().equals("FLOAT")) {
                terminal = 904;
            }
            if (term.trim().equals("CHAR")) {
                terminal = 905;
            }
            if (term.trim().equals("STRING")) {
                terminal = 906;
            }
            if (term.trim().equals("BOOLEAN")) {
                terminal = 907;
            }
            if (term.trim().equals("READ")) {
                terminal = 908;
            }
            if (term.trim().equals("PRINT")) {
                terminal = 909;
            }
            if (term.trim().equals("IF")) {
                terminal = 910;
            }
            if (term.trim().equals("ENDIF")) {
                terminal = 911;
            }
            if (term.trim().equals("ELSE")) {
                terminal = 912;
            }
            if (term.trim().equals("WHILE")) {
                terminal = 913;
            }
            if (term.trim().equals("ENDWHILE")) {
                terminal = 914;
            }
            if (term.trim().equals("FOR")) {
                terminal = 915;
            }
            if (term.trim().equals("ENDFOR")) {
                terminal = 916;
            }
            if (term.trim().equals("TRUE")) {
                terminal = 941;
            }
            if (term.trim().equals("FALSE")) {
                terminal = 942;
            }
            if (term.trim().equals("PUBLIC")) {
                terminal = 943;
            }
            if (terminal == 0) {
                outputArea.append(analiza.substring(0, analiza.length() - 1) + ": Identificador\n");
                analiza = analiza.substring(0, analiza.length() -1);
                terminal = 930;
            } else {

                outputArea.append(analiza.substring(0, analiza.length() - 1) + ": Palabra Reservada\n");
                analiza = analiza.substring(0, analiza.length() -1);
            }

            inputStream.add(Integer.toString(terminal));

        }

        if (estado==101){
            if(puntero == word.length()&&  analiza.charAt(analiza.length()-1) >= 'a'&& analiza.charAt(analiza.length()-1) <= 'z'
                    ||  analiza.charAt(analiza.length()-1) >= 'A'&& analiza.charAt(analiza.length()-1) <= 'Z'
                    ||  analiza.charAt(analiza.length()-1) >= '0'&& analiza.charAt(analiza.length()-1) <= '9')
            {
                analiza += " ";
            }else{
                puntero = punteroAnterior;
            }
            outputArea.append(analiza.substring(0, analiza.length() - 1) + ": Identificador\n");
            terminal = 930;
            inputStream.add(Integer.toString(terminal));
            analiza = analiza.substring(0, analiza.length() - 1);
        }

        if(estado == 102){
            if(puntero == word.length() && analiza.charAt(analiza.length()-1) >= '0'&& analiza.charAt(analiza.length()-1) <= '9')
            {
                analiza += " ";
            }else{
                puntero = punteroAnterior;
            }
            outputArea.append(analiza.substring(0, analiza.length() - 1) + ": Entero\n");
            analiza = analiza.substring(0, analiza.length() - 1);
            terminal = 936;
            inputStream.add(Integer.toString(terminal));
        }
        if(estado == 103){
            if(puntero == word.length() && analiza.charAt(analiza.length()-1) >= '0'&& analiza.charAt(analiza.length()-1) <= '9')
            {

            }else{
                puntero = punteroAnterior;
            }
            outputArea.append(analiza.substring(0, analiza.length() - 1) + ": Numero real\n");
            analiza = analiza.substring(0, analiza.length() - 1);
            terminal = 937;
            inputStream.add(Integer.toString(terminal));
        }
        if(estado == 104){
            if(puntero == word.length()-1)
            {
            }else{
                puntero = punteroAnterior;
            }
            outputArea.append(analiza.substring(0, analiza.length() - 1) + ": Numero con notacion cientifica\n");
            analiza = analiza.substring(0, analiza.length() - 1);
            terminal = 938;
            inputStream.add(Integer.toString(terminal));
        }
        if(estado == 105){
            outputArea.append(analiza + ": Operador aritmetico suma\n");
            terminal = 932;
            inputStream.add(Integer.toString(terminal));
        }
        if(estado == 106){
            outputArea.append(analiza + ": Operador aritmetico resta\n");
            terminal = 933;
            inputStream.add(Integer.toString(terminal));
        }
        if(estado == 107){
            outputArea.append(analiza + ": Operador aritmetico multiplicacion\n");
            terminal = 934;
            inputStream.add(Integer.toString(terminal));
        }
        if(estado == 108){
            outputArea.append(analiza + ": Operador aritmetico division\n");
            terminal = 935;
            inputStream.add(Integer.toString(terminal));
        }
        if(estado == 109){
            outputArea.append(analiza + ": Operador aritmetico modulo\n");
        }
        if(estado == 110){
            outputArea.append(analiza + ": Operador logico AND\n");
            terminal = 918;
            inputStream.add(Integer.toString(terminal));
        }
        if(estado == 111){
            outputArea.append(analiza + ": Operador logico OR\n");
            terminal = 917;
            inputStream.add(Integer.toString(terminal));
        }
        if(estado == 112){
            if(puntero == word.length()-1){
                analiza += " ";
            }else{
                puntero = punteroAnterior;
            }
            outputArea.append(analiza + ": Operador logico NOT\n");
            terminal = 919;
            inputStream.add(Integer.toString(terminal));
            analiza = analiza.substring(0, analiza.length() - 1);

        }
        if(estado == 113){
            outputArea.append(analiza + ": Operador relacional !=\n");
            terminal = 920;
            inputStream.add(Integer.toString(terminal));
        }
        if(estado == 114){
            if(puntero == word.length()-1){
                analiza += " ";
            }
            outputArea.append(analiza.substring(0, analiza.length()) + ": Operador relacional de igual a\n");
            terminal = 931;
            inputStream.add(Integer.toString(terminal));
            //analiza = analiza.substring(0, analiza.length() - 1);
        }
        if(estado == 115){
            if(puntero == word.length()-1){
                analiza += " ";
            }else{
                puntero = punteroAnterior;
            }
            outputArea.append(analiza.substring(0, analiza.length() - 1) + ": Operador relacional de asignacion\n");
            terminal = 926;
            inputStream.add(Integer.toString(terminal));
            analiza = analiza.substring(0, analiza.length() - 1);
        }
        if(estado == 116){
            if(puntero == word.length()-1){
                analiza += " ";
            }else{
                puntero = punteroAnterior;
            }
            outputArea.append(analiza.substring(0, analiza.length() - 1) + ": Operador relacional menor que\n");
            terminal = 921;
            inputStream.add(Integer.toString(terminal));
            analiza = analiza.substring(0, analiza.length() - 1);
        }
        if(estado == 117){
            outputArea.append(analiza + ": Operador relacional menor igual que \n");
            terminal = 922;
            inputStream.add(Integer.toString(terminal));
        }
        if(estado == 119){
            if(puntero == word.length()-1)
            {
                analiza += " ";
            }else{
                puntero = punteroAnterior;
            }
            outputArea.append(analiza.substring(0, analiza.length() - 1) + ": Operador relacional mayor que\n");
            terminal = 923;
            inputStream.add(Integer.toString(terminal));
            analiza = analiza.substring(0, analiza.length() - 1);
        }
        if(estado == 118){
            outputArea.append(analiza + ": Operador relacional mayor igual que\n");
            terminal = 924;
            inputStream.add(Integer.toString(terminal));
        }
        if(estado == 120){
            outputArea.append(analiza + ": Constante caracter\n");
            terminal = 939;
            inputStream.add(Integer.toString(terminal));
        }

        if(estado == 121){
            outputArea.append(analiza + ": Constante String\n");
            terminal = 940;
            inputStream.add(Integer.toString(terminal));
        }
        if(estado == 122){
            outputArea.append(analiza + ": Simbolo de agrupacion parentesis abierto (\n");
            terminal = 928;
            inputStream.add(Integer.toString(terminal));
        }
        if(estado == 123){
            outputArea.append(analiza + ": Simbolo de agrupacion parentesis cerrado )\n");
            terminal = 929;
            inputStream.add(Integer.toString(terminal));
        }
        if(estado == 124){
            outputArea.append(analiza + ": Simbolo de agrupacion corchete abre [\n");
        }
        if(estado == 125){
            outputArea.append(analiza + ": Simbolo de agrupacion corchete cierra ]\n");
        }
        if(estado == 126){
            outputArea.append(analiza + ": Signo de puntuacion punto y coma\n");
            terminal = 927;
            inputStream.add(Integer.toString(terminal));
        }
        if(estado == 127){
            outputArea.append(analiza + ": Signo de puntuacion coma\n");
            terminal = 925;
            inputStream.add(Integer.toString(terminal));
        }
        if(estado == 128){
            outputArea.append(analiza.substring(0, analiza.length() - 1) + ": Comentario en linea\n");
            analiza = analiza.substring(0, analiza.length() - 1);
            terminal = 9999;
        }
        if(estado == 500){
            outputArea.append(analiza + ": Error de inicio\n");
        }
        if(estado == 501){
            if(puntero == word.length()-1){
            }else{
                puntero = punteroAnterior;
            }
            outputArea.append(analiza.substring(0, analiza.length() - 1) + ": Error real, se espera un n�mero y se recibio algo diferente\n");
            analiza = analiza.substring(0, analiza.length() - 1);
        }
        if(estado == 502){
            if(puntero == word.length()-1){
            }else{
                puntero = punteroAnterior;
            }
            outputArea.append(analiza.substring(0, analiza.length() - 1) + ": Error de notacion, se espera un + o - o digito  y se recibio algo diferente\n");
            analiza = analiza.substring(0, analiza.length() - 1);
        }
        if(estado == 503){
            if(puntero == word.length()-1){
                analiza += " ";
            }else{
                puntero = punteroAnterior;
            }
            outputArea.append(analiza.substring(0, analiza.length() - 1) + ": Error notacion, se espera un numero \n");
            analiza = analiza.substring(0, analiza.length() - 1);
        }
        if(estado == 504){
            if(puntero == word.length()){
                analiza += " ";
            }else{
                puntero = punteroAnterior;
            }
            outputArea.append(analiza.substring(0, analiza.length() - 1) + ": Error de operacion logico, se espera un & \n");
            analiza = analiza.substring(0, analiza.length() - 1);
        }
        if(estado == 505){
            if(puntero == word.length()){
                analiza += " ";
            }else{
                puntero = punteroAnterior;
            }
            outputArea.append(analiza.substring(0, analiza.length() - 1) + ": Error de operacion logico se esperaba un |\n");
            analiza = analiza.substring(0, analiza.length() - 1);
        }
        if(estado == 506){
            if(puntero == word.length()){
                analiza += " ";
            }else{
                puntero = punteroAnterior;
            }
            outputArea.append(analiza.substring(0, analiza.length() - 1) + ": Error, se espera algo diferente de comilla\n");
            analiza = analiza.substring(0, analiza.length() - 1);
        }
        if(estado == 507){
            if(puntero == word.length()){
                analiza += " ";
            }else{
                puntero = punteroAnterior;
            }
            outputArea.append(analiza.substring(0, analiza.length() - 1) + ": Error, se espera finalizar con comilla\n");
            analiza = analiza.substring(0, analiza.length() - 1);
        }
        if(estado == 1000){
            outputArea.append(analiza.substring(0, analiza.length() - 1) + ": Error, no se cerro la cadena String\n");
            analiza = analiza.substring(0, analiza.length() - 1);
        }
        variable = analiza.trim();
        //JOptionPane.showMessageDialog(null,variable);
        return terminal;
    }

    public String dameCodigo() throws IOException {

        String Word="";

        String PathFile ="read.txt";

        File file = new File(PathFile);
        FileReader fileReader=null;
        fileReader = new FileReader(file);
        int cont = 0;
        while (cont<=file.length()-1) {
            int caracter = fileReader.read();
            Word += (char) caracter;
            cont++;
        }

        return Word+=" ";}

    public void Compilar(JTextArea outputArea)
    {
        tablas = new Tablas();
        int Estado;
        int Col;
        int FinalTerm = 0;
        String Word="";

        String PathFile ="read.txt";

        File file = new File(PathFile);
        FileReader fileReader=null;
        try {
            fileReader = new FileReader(file);
            int cont = 0;
            while (cont<=file.length()-1) {
                int caracter = fileReader.read();
                Word += (char) caracter;
                cont++;
            }

            int aux;
            String VarAnalysis;
            int i = 0;
            while(i<Word.length())
            {
                aux = i-1;
                Estado = 0;
                VarAnalysis = "";
                while (Estado <= 18) {
                    int caracter = Word.charAt(i);
                    VarAnalysis += (char) caracter;
                    Col = Link((char) caracter);
                    //Estado = tablas.Tabla_lexico[Estado][Col];
                    Estado = tablas.tablaLexico(Estado,Col);
                    i++;
                    aux++;
                    if(i == Word.length() && Estado <= 18 ){
                        //Estado = tablas.Tabla_lexico[Estado][27];;
                        Estado = tablas.tablaLexico(Estado,27);
                    }
                }
                if(Estado == 100)
                {
                    if(i == Word.length()&&  VarAnalysis.charAt(VarAnalysis.length()-1) >= 'A'&& VarAnalysis.charAt(VarAnalysis.length()-1) <= 'Z') {
                        VarAnalysis += " ";
                    }else{
                        i = aux;
                    }
                    String term = VarAnalysis.substring(0,VarAnalysis.length()-1);
                    if(term.trim().equals("CLASS")){
                        FinalTerm = 900;
                    }
                    if(term.trim().equals("BEGIN")){
                        FinalTerm = 901;
                    }
                    if(term.trim().equals("ENDCLASS")){
                        FinalTerm = 902;
                    }
                    if(term.trim().equals("INTEGER")){
                        FinalTerm = 903;
                    }
                    if(term.trim().equals("FLOAT")){
                        FinalTerm = 904;
                    }
                    if(term.trim().equals("CHAR")){
                        FinalTerm = 905;
                    }
                    if(term.trim().equals("STRING")){
                        FinalTerm = 906;
                    }
                    if(term.trim().equals("BOOLEAN")){
                        FinalTerm = 907;
                    }
                    if(term.trim().equals("READ")){
                        FinalTerm = 908;
                    }
                    if(term.trim().equals("PRINT")){
                        FinalTerm = 909;
                    }
                    if(term.trim().equals("IF")){
                        FinalTerm = 910;
                    }
                    if(term.trim().equals("ENDIF")){
                        FinalTerm = 911;
                    }
                    if(term.trim().equals("ELSE")){
                        FinalTerm = 912;
                    }
                    if(term.trim().equals("WHILE")){
                        FinalTerm = 913;
                    }
                    if(term.trim().equals("ENDWHILE")){
                        FinalTerm = 914;
                    }
                    if(term.trim().equals("FOR")){
                        FinalTerm = 915;
                    }
                    if(term.trim().equals("ENDFOR")){
                        FinalTerm = 916;
                    }
                    if(term.trim().equals("TRUE")){
                        FinalTerm = 941;
                    }
                    if(term.trim().equals("FALSE")){
                        FinalTerm = 942;
                    }
                    if(term.trim().equals("PUBLIC")){
                        FinalTerm = 943;
                    }
                    if(FinalTerm == 0){
                        outputArea.append(VarAnalysis.substring(0, VarAnalysis.length() - 1) + ": Identificador\n");
                        FinalTerm = 930;
                    }
                    else{
                        outputArea.append(VarAnalysis.substring(0, VarAnalysis.length() - 1) + ": Palabra Reservada\n");
                        VarAnalysis += " ";
                    }
                    inputStream.add(Integer.toString(FinalTerm));
                    FinalTerm = 0;

                }
                if(Estado == 101){
                    if(i == Word.length()&&  VarAnalysis.charAt(VarAnalysis.length()-1) >= 'a'&& VarAnalysis.charAt(VarAnalysis.length()-1) <= 'z'
                            ||  VarAnalysis.charAt(VarAnalysis.length()-1) >= 'A'&& VarAnalysis.charAt(VarAnalysis.length()-1) <= 'Z'
                            ||  VarAnalysis.charAt(VarAnalysis.length()-1) >= '0'&& VarAnalysis.charAt(VarAnalysis.length()-1) <= '9')
                    {
                        VarAnalysis += " ";
                    }else{
                        i = aux;
                    }
                    outputArea.append(VarAnalysis.substring(0, VarAnalysis.length() - 1) + ": Identificador\n");
                    FinalTerm = 930;
                    inputStream.add(Integer.toString(FinalTerm));
                    FinalTerm = 0;
                }
                if(Estado == 102){
                    if(i == Word.length() && VarAnalysis.charAt(VarAnalysis.length()-1) >= '0'&& VarAnalysis.charAt(VarAnalysis.length()-1) <= '9')
                    {
                        VarAnalysis += " ";
                    }else{
                        i = aux;
                    }
                    outputArea.append(VarAnalysis.substring(0, VarAnalysis.length() - 1) + ": Entero\n");
                    FinalTerm = 936;
                    inputStream.add(Integer.toString(FinalTerm));
                    FinalTerm = 0;
                }
                if(Estado == 103){
                    if(i == Word.length() && VarAnalysis.charAt(VarAnalysis.length()-1) >= '0'&& VarAnalysis.charAt(VarAnalysis.length()-1) <= '9')
                    {}else{
                        i = aux;
                    }
                    outputArea.append(VarAnalysis.substring(0, VarAnalysis.length() - 1) + ": Numero real\n");
                    FinalTerm = 937;
                    inputStream.add(Integer.toString(FinalTerm));
                    FinalTerm = 0;
                }
                if(Estado == 104){
                    if(i == Word.length()-1)
                    {
                    }else{
                        i = aux;
                    }
                    outputArea.append(VarAnalysis.substring(0, VarAnalysis.length() - 1) + ": Numero con notacion cientifica\n");
                    FinalTerm = 938;
                    inputStream.add(Integer.toString(FinalTerm));
                    FinalTerm = 0;
                }
                if(Estado == 105){
                    outputArea.append(VarAnalysis + ": Operador aritmetico suma\n");
                    FinalTerm = 932;
                    inputStream.add(Integer.toString(FinalTerm));
                    FinalTerm = 0;
                }
                if(Estado == 106){
                    outputArea.append(VarAnalysis + ": Operador aritmetico resta\n");
                    FinalTerm = 933;
                    inputStream.add(Integer.toString(FinalTerm));
                    FinalTerm = 0;
                }
                if(Estado == 107){
                    outputArea.append(VarAnalysis + ": Operador aritmetico multiplicacion\n");
                    FinalTerm = 934;
                    inputStream.add(Integer.toString(FinalTerm));
                    FinalTerm = 0;
                }
                if(Estado == 108){
                    outputArea.append(VarAnalysis + ": Operador aritmetico division\n");
                    FinalTerm = 935;
                    inputStream.add(Integer.toString(FinalTerm));
                    FinalTerm = 0;
                }
                if(Estado == 109){
                    outputArea.append(VarAnalysis + ": Operador aritmetico modulo\n");
                }
                if(Estado == 110){
                    outputArea.append(VarAnalysis + ": Operador logico AND\n");
                    FinalTerm = 918;
                    inputStream.add(Integer.toString(FinalTerm));
                    FinalTerm = 0;
                }
                if(Estado == 111){
                    outputArea.append(VarAnalysis + ": Operador logico OR\n");
                    FinalTerm = 917;
                    inputStream.add(Integer.toString(FinalTerm));
                    FinalTerm = 0;
                }
                if(Estado == 112){
                    outputArea.append(VarAnalysis + ": Operador relacional !\n");
                    FinalTerm = 919;
                    inputStream.add(Integer.toString(FinalTerm));
                    FinalTerm = 0;
                }
                if(Estado == 113){
                    outputArea.append(VarAnalysis + ": Operador relacional !=\n");
                    FinalTerm = 920;
                    inputStream.add(Integer.toString(FinalTerm));
                    FinalTerm = 0;
                }
                if(Estado == 114){
                    if(i == Word.length()-1){
                        VarAnalysis += " ";
                    }
                    outputArea.append(VarAnalysis.substring(0, VarAnalysis.length()) + ": Operador relacional de igual a\n");
                    FinalTerm = 931;
                    inputStream.add(Integer.toString(FinalTerm));
                    FinalTerm = 0;
                }
                if(Estado == 115){
                    if(i == Word.length()-1){
                        VarAnalysis += " ";
                    }else{
                        i = aux;
                    }
                    outputArea.append(VarAnalysis.substring(0, VarAnalysis.length() - 1) + ": Operador relacional de asignacion\n");
                    FinalTerm = 926;
                    inputStream.add(Integer.toString(FinalTerm));
                    FinalTerm = 0;
                }
                if(Estado == 116){
                    if(i == Word.length()-1){
                        VarAnalysis += " ";
                    }else{
                        i = aux;
                    }
                    outputArea.append(VarAnalysis.substring(0, VarAnalysis.length() - 1) + ": Operador relacional menor que\n");
                    FinalTerm = 921;
                    inputStream.add(Integer.toString(FinalTerm));
                    FinalTerm = 0;
                }
                if(Estado == 117){
                    outputArea.append(VarAnalysis + ": Operador relacional menor igual que \n");
                    FinalTerm = 922;
                    inputStream.add(Integer.toString(FinalTerm));
                    FinalTerm = 0;
                }
                if(Estado == 119){
                    if(i == Word.length()-1)
                    {
                        VarAnalysis += " ";
                    }else{
                        i = aux;
                    }
                    outputArea.append(VarAnalysis.substring(0, VarAnalysis.length() - 1) + ": Operador relacional mayor que\n");
                    FinalTerm = 923;
                    inputStream.add(Integer.toString(FinalTerm));
                    FinalTerm = 0;
                }
                if(Estado == 118){
                    outputArea.append(VarAnalysis + ": Operador relacional mayor igual que\n");
                    FinalTerm = 924;
                    inputStream.add(Integer.toString(FinalTerm));
                    FinalTerm = 0;
                }
                if(Estado == 120){
                    outputArea.append(VarAnalysis + ": Constante caracter\n");
                    FinalTerm = 939;
                    inputStream.add(Integer.toString(FinalTerm));
                    FinalTerm = 0;
                }

                if(Estado == 121){
                    outputArea.append(VarAnalysis + ": Constante String\n");
                    FinalTerm = 940;
                    inputStream.add(Integer.toString(FinalTerm));
                    FinalTerm = 0;
                }
                if(Estado == 122){
                    outputArea.append(VarAnalysis + ": Simbolo de agrupacion parentesis abierto (\n");
                    FinalTerm = 928;
                    inputStream.add(Integer.toString(FinalTerm));
                    FinalTerm = 0;
                }
                if(Estado == 123){
                    outputArea.append(VarAnalysis + ": Simbolo de agrupacion parentesis cerrado )\n");
                    FinalTerm = 929;
                    inputStream.add(Integer.toString(FinalTerm));
                    FinalTerm = 0;
                }
                if(Estado == 124){
                    outputArea.append(VarAnalysis + ": Simbolo de agrupacion corchete abre [\n");
                }
                if(Estado == 125){
                    outputArea.append(VarAnalysis + ": Simbolo de agrupacion corchete cierra ]\n");
                }
                if(Estado == 126){
                    outputArea.append(VarAnalysis + ": Signo de puntuacion punto y coma\n");
                    FinalTerm = 927;
                    inputStream.add(Integer.toString(FinalTerm));
                    FinalTerm = 0;
                }
                if(Estado == 127){
                    outputArea.append(VarAnalysis + ": Signo de puntuacion coma\n");
                    FinalTerm = 925;
                    inputStream.add(Integer.toString(FinalTerm));
                    FinalTerm = 0;
                }
                if(Estado == 128){
                    outputArea.append(VarAnalysis.substring(0, VarAnalysis.length() - 1) + ": Comentario en linea\n");
                }
                if(Estado == 500){
                    outputArea.append(VarAnalysis + ": Error de inicio\n");
                }
                if(Estado == 501){
                    if(i == Word.length()-1){
                    }else{
                        i = aux;
                    }
                    outputArea.append(VarAnalysis.substring(0, VarAnalysis.length() - 1) + ": Error real, se espera un n�mero y se recibio algo diferente\n");
                }
                if(Estado == 502){
                    if(i == Word.length()-1){
                    }else{
                        i = aux;
                    }
                    outputArea.append(VarAnalysis.substring(0, VarAnalysis.length() - 1) + ": Error de notacion, se espera un + o - o digito  y se recibio algo diferente\n");
                }
                if(Estado == 503){
                    if(i == Word.length()-1){
                        VarAnalysis += " ";
                    }else{
                        i = aux;
                    }
                    outputArea.append(VarAnalysis.substring(0, VarAnalysis.length() - 1) + ": Error notacion, se espera un numero \n");
                }
                if(Estado == 504){
                    if(i == Word.length()){
                        VarAnalysis += " ";
                    }else{
                        i = aux;
                    }
                    outputArea.append(VarAnalysis.substring(0, VarAnalysis.length() - 1) + ": Error de operacion logico, se espera un & \n");
                }
                if(Estado == 505){
                    if(i == Word.length()){
                        VarAnalysis += " ";
                    }else{
                        i = aux;
                    }
                    outputArea.append(VarAnalysis.substring(0, VarAnalysis.length() - 1) + ": Error de operacion logico se esperaba un |\n");
                }
                if(Estado == 506){
                    if(i == Word.length()){
                        VarAnalysis += " ";
                    }else{
                        i = aux;
                    }
                    outputArea.append(VarAnalysis.substring(0, VarAnalysis.length() - 1) + ": Error, se espera algo diferente de comilla\n");
                }
                if(Estado == 507){
                    if(i == Word.length()){
                        VarAnalysis += " ";
                    }else{
                        i = aux;
                    }
                    outputArea.append(VarAnalysis.substring(0, VarAnalysis.length() - 1) + ": Error, se espera finalizar con comilla\n");
                }
                if(Estado == 1000){
                    outputArea.append(VarAnalysis.substring(0, VarAnalysis.length() - 1) + ": Error, no se cerro la cadena String\n");
                }
            }
        }catch (FileNotFoundException e){}
        catch (IOException e) {}
        finally {
            try {
                if(fileReader !=null){
                    fileReader.close();
                }
            }catch (Exception e){}
        }
    }

    public static int Link(char caracter)
    {
        int character = 27;

        if(caracter >= 'a' && caracter <= 'z'){
            character = 0;
        }
        if(caracter >= 'A' && caracter <= 'Z'){
            character = 1;
        }
        if(caracter >= '0' && caracter <= '9'){
            character = 2;
        }
        switch (caracter) {
            case '_':
                return 3;
            case '.':
                return 4;
            case '\t':
                return 29;
            case '\n':
                return 30;
            case '+':
                return 7;
            case '*':
                return 9;
            case '/':
                return 10;
            case '%':
                return 11;
            case '&':
                return 12;
            case '|':
                return 13;
            case '!':
                return 14;
            case '=':
                return 15;
            case '<':
                return 16;
            case '>':
                return 17;
            case '"':
                return 19;
            case '(':
                return 20;
            case ')':
                return 21;
            case '[':
                return 22;
            case ']':
                return 23;
            case ';':
                return 24;
            case ',':
                return 25;
            case '#':
                return 26;
        }
        if(caracter == 'E'){
            character = 5;
        }
        if(caracter == 'e'){
            character = 6;
        }
        if((int)caracter == 45){
            return 8;
        }
        if((int)caracter == 39){
            return 18;
        }
        if(caracter == 32){
            return 28;
        }
        return character;
    }

    public void Unpack(JTextArea inputArea){
        FileWriter fw = null;
        PrintWriter pw = null;
        try
        {
            fw = new FileWriter("read.txt");
            pw = new PrintWriter(fw);
            String cadena = inputArea.getText();
            pw.write(cadena);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != fw)
                    fw.close();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public static void marcarError(String palabra){
         if (palabra.length() >= 1){
             DefaultHighlighter.DefaultHighlightPainter higlightPainter = new DefaultHighlighter.DefaultHighlightPainter(Color.RED);
             Highlighter h = inputArea.getHighlighter();
             h.removeAllHighlights();
             String text = inputArea.getText();
             String caracteres = palabra;
             Pattern p = Pattern.compile("(?i)"+palabra);
             Matcher m = p.matcher(text);
             while (m.find()){
                 try{

                     h.addHighlight(m.start(),m.end(), higlightPainter);


                 }   catch (BadLocationException ble){

                 }
             }
         }
    }


    public void ejecuta(){
        ic = 0;
       if (programa.getCuadruplos() != null && programa.getCuadruplosTabla() != null ){
           tabla_ejecucion = new ArrayList<SimboloEjecucion>();
           hiloEjecucion = new Thread(new Runnable() {
               @Override
               public void run() {

                   ArrayList<Cuadruplo> cuadruplos = programa.getCuadruplos();
                   ArrayList<Simbolo> tabla_simbolos = programa.getTabla_simbolos();
                   ArrayList<Simbolo> tabla_constantes = programa.getTabla_constantes();
                   //List<Simbolo> constantesString = programa.getConstantesTabla();
                   //List<Simbolo> simbolosString = programa.getSimbolosTabla();
                   List<Cuadruplo> cuadruplosString = programa.getCuadruplosTabla();

                   for (ic=0; ic < cuadruplos.size(); ic++){

                       int codigoOperacion = cuadruplos.get(ic).getCodoper();
                       switch (codigoOperacion) {
                           // *** GOTO ***
                           case 1:
                               if (codigoOperacion == 1){
                                  int salto;
                                   salto = cuadruplos.get(ic).getRes();
                                   if (salto > cuadruplos.size()){
                                       ic = salto;
                                       //JOptionPane.showMessageDialog(null,ic);
                                       continue;
                                   } else {
                                       ic = salto-2;
                                       //JOptionPane.showMessageDialog(null,ic);
                                       continue;
                                   }
                               }
                               break;
                           // *** GOTOF ***
                           case 2:
                               if (codigoOperacion == 2) {

                                   String valor = "";
                                   boolean temp;
                                   for (Simbolo s : tabla_simbolos) {
                                       if (s.getDescrip().equals(cuadruplosString.get(ic).getOp1S())) {
                                           int tipo = s.getTipo();
                                           switch (tipo) {
                                               case 907:
                                                   if (s.getValor().equals("")){
                                                       valor = s.getDescrip();
                                                   }  else
                                                       valor = s.getValor();
                                                   break;
                                               default:
                                                   JOptionPane.showMessageDialog(null,"Tipo de datos incompatible");
                                                   break;
                                           }
                                       }
                                   }  //todo quitar este for

                                   for (Simbolo c : tabla_constantes) {
                                       if (c.getDescrip().equals(cuadruplosString.get(ic).getOp1S())) {
                                           int tipo = c.getTipo();
                                           switch (tipo) {
                                               case 907:
                                                   if (c.getValor().equals("")){
                                                      valor = c.getDescrip();
                                                   }  else
                                                    valor = c.getValor();
                                                   break;
                                               default:
                                                   JOptionPane.showMessageDialog(null,"Tipo de datos incompatible");
                                                   break;
                                           }
                                       }
                                   }

                                   if (valor.equals("TRUE")){
                                       temp = true;
                                   } else temp = false;

                                   if (!temp) {
                                       int salto;
                                       salto = cuadruplos.get(ic).getRes();
                                       if (salto > cuadruplos.size()){
                                           ic = salto-2;
                                           continue;
                                       } else {
                                           ic=salto-2;
                                           continue;
                                       }
                                   } else {
                                     continue;
                                   }

                               }
                               break;

                           // *** INPUT ***
                           case 908:
                               if (!pausado) {
                                   if (hiloEjecucion.isAlive()){
                                       pausado=true;
                                       hiloEjecucion.suspend();
                                       try {
                                           Thread.sleep(1);
                                       } catch (InterruptedException e) {
                                           e.printStackTrace();
                                       }
                                   } else
                                   if (hiloEjecucion.isDaemon()){
                                       JOptionPane.showMessageDialog(null,"No se ha iniciado la ejecucion");
                                   }
                               }
                               break;
                           // *** OUTPUT ***
                           case 909:
                               punteroActual = consoleArea.getText().length();
                               /*Revisar si lo que se imprimira es una constante o una variable si es variable sacar de tabla de simbolos*/
                               if (cuadruplos.get(ic).getRes() == 930) {
                                  for (Simbolo s : tabla_simbolos){
                                       if (s.getDescrip().equals(cuadruplosString.get(ic).getResS()) ){
                                           consoleArea.append(" "+s.getValor()+"\n");
                                       }
                                  }
                               } else  {
                                   for (Simbolo c : tabla_constantes){
                                       if (c.getDescrip().equals(cuadruplosString.get(ic).getResS())){
                                           if (c.getValor().equals("")){
                                              consoleArea.append(" "+c.getDescrip().substring(1,c.getDescrip().length()-1)+"\n");
                                           } else consoleArea.append(" "+c.getValor()+"\n");

                                          // consoleArea.append(" "+c.get(ic).getResS().substring(1,cuadruplosString.get(ic).getResS().length()-1)+"\n");
                                       }
                                   }
                               }

                               consoleArea.append(">");
                               consoleArea.setCaretPosition(consoleArea.getText().length());
                               punteroAnterior = consoleArea.getText().length();
                               break;
                           // *** SUMA ***
                           case 932:
                               /*Revisar si es constante o simbolo*/
                               if (codigoOperacion == 932){
                                   float op1=0,op2=0;
                                   for (Simbolo s : tabla_simbolos){
                                   /*Sacar el operando 1*/
                                       if (s.getDescrip().equals(cuadruplosString.get(ic).getOp1S())){
                                           int tipo = s.getTipo();
                                           switch (tipo){
                                               case 903:
                                                   op1=Float.parseFloat(s.getValor());
                                                   break;
                                               case 904:
                                                   op1=Float.parseFloat(s.getValor());
                                                   break;
                                               default:
                                                   break;
                                           }
                                       }
                                   /*Sacar el operando 2*/
                                       if (s.getDescrip().equals(cuadruplosString.get(ic).getOp2S())){
                                           int tipo = s.getTipo();
                                           switch (tipo){
                                               case 903:
                                                   op2=Float.parseFloat(s.getValor());
                                                   break;
                                               case 904:
                                                   op2=Float.parseFloat(s.getValor());
                                                   break;
                                               default:
                                                   break;
                                           }
                                       }
                                   }

                                   for (Simbolo c : tabla_constantes){
                                   /*Sacar el operando 1*/
                                       if (c.getDescrip().equals(cuadruplosString.get(ic).getOp1S())){
                                           int tipo = c.getTipo();
                                           switch (tipo){
                                               case 903:
                                                   if (c.getValor().equals("")){
                                                       op1=Float.parseFloat(c.getDescrip());
                                                   } else op1=Float.parseFloat(c.getValor());
                                                   break;
                                               case 904:
                                                   if (c.getValor().equals("")){
                                                       op1=Float.parseFloat(c.getDescrip());
                                                   } else op1=Float.parseFloat(c.getValor());
                                                   break;
                                               default:
                                                   break;
                                           }
                                       }
                                   /*Sacar el operando 2*/
                                       if (c.getDescrip().equals(cuadruplosString.get(ic).getOp2S())){
                                           int tipo = c.getTipo();
                                           switch (tipo){
                                               case 903:
                                                   if (c.getValor().equals("")){
                                                       op2=Float.parseFloat(c.getDescrip());
                                                   } else op2=Float.parseFloat(c.getValor());
                                                   break;
                                               case 904:
                                                   if (c.getValor().equals("")){
                                                       op2=Float.parseFloat(c.getDescrip());
                                                   } else op2=Float.parseFloat(c.getValor());
                                                   break;
                                               default:
                                                   break;
                                           }
                                       }
                                   }
                                   float res = op1 + op2;
                                   for (Simbolo s : tabla_constantes){
                                       if (s.getDescrip().equals(cuadruplosString.get(ic).getResS())){
                                           s.setValor(String.valueOf(res));
                                       }
                                   }
                               }
                               break;

                           // *** RESTA ***
                           case 933:
                               /*Revisar si es constante o simbolo*/
                               if (codigoOperacion == 933){
                                   float op1=0,op2=0;
                                   for (Simbolo s : tabla_simbolos){
                                   /*Sacar el operando 1*/
                                       if (s.getDescrip().equals(cuadruplosString.get(ic).getOp1S())){
                                           int tipo = s.getTipo();
                                           switch (tipo){
                                               case 903:
                                                   op1=Float.parseFloat(s.getValor());
                                                   break;
                                               case 904:
                                                   op1=Float.parseFloat(s.getValor());
                                                   break;
                                               default:
                                                   break;
                                           }
                                       }
                                   /*Sacar el operando 2*/
                                       if (s.getDescrip().equals(cuadruplosString.get(ic).getOp2S())){
                                           int tipo = s.getTipo();
                                           switch (tipo){
                                               case 903:
                                                   op2=Float.parseFloat(s.getValor());
                                                   break;
                                               case 904:
                                                   op2=Float.parseFloat(s.getValor());
                                                   break;
                                               default:
                                                   break;
                                           }
                                       }
                                   }

                                   for (Simbolo c : tabla_constantes){
                                   /*Sacar el operando 1*/
                                       if (c.getDescrip().equals(cuadruplosString.get(ic).getOp1S())){
                                           int tipo = c.getTipo();
                                           switch (tipo){
                                               case 903:
                                                   if (c.getValor().equals("")){
                                                       op1=Float.parseFloat(c.getDescrip());
                                                   } else op1=Float.parseFloat(c.getValor());
                                                   break;
                                               case 904:
                                                   if (c.getValor().equals("")){
                                                       op1=Float.parseFloat(c.getDescrip());
                                                   } else op1=Float.parseFloat(c.getValor());
                                                   break;
                                               default:
                                                   break;
                                           }
                                       }
                                   /*Sacar el operando 2*/
                                       if (c.getDescrip().equals(cuadruplosString.get(ic).getOp2S())){
                                           int tipo = c.getTipo();
                                           switch (tipo){
                                               case 903:
                                                   if (c.getValor().equals("")){
                                                       op2=Float.parseFloat(c.getDescrip());
                                                   } else op2=Float.parseFloat(c.getValor());
                                                   break;
                                               case 904:
                                                   if (c.getValor().equals("")){
                                                       op2=Float.parseFloat(c.getDescrip());
                                                   } else op2=Float.parseFloat(c.getValor());
                                                   break;
                                               default:
                                                   break;
                                           }
                                       }
                                   }
                                   float res = op1 - op2;
                                   for (Simbolo s : tabla_constantes){
                                       if (s.getDescrip().equals(cuadruplosString.get(ic).getResS())){
                                           s.setValor(String.valueOf(res));
                                       }
                                   }
                               }

                               break;

                           // *** MULTIPLICACION ***
                           case 934:
                               /*Revisar si es constante o simbolo*/
                               if (codigoOperacion == 934){
                                   float op1=0,op2=0;
                                   for (Simbolo s : tabla_simbolos){
                                   /*Sacar el operando 1*/
                                       if (s.getDescrip().equals(cuadruplosString.get(ic).getOp1S())){
                                           int tipo = s.getTipo();
                                           switch (tipo){
                                               case 903:
                                                   op1=Float.parseFloat(s.getValor());
                                                   break;
                                               case 904:
                                                   op1=Float.parseFloat(s.getValor());
                                                   break;
                                               default:
                                                   break;
                                           }
                                       }
                                   /*Sacar el operando 2*/
                                       if (s.getDescrip().equals(cuadruplosString.get(ic).getOp2S())){
                                           int tipo = s.getTipo();
                                           switch (tipo){
                                               case 903:
                                                   op2=Float.parseFloat(s.getValor());
                                                   break;
                                               case 904:
                                                   op2=Float.parseFloat(s.getValor());
                                                   break;
                                               default:
                                                   break;
                                           }
                                       }
                                   }

                                   for (Simbolo c : tabla_constantes){
                                   /*Sacar el operando 1*/
                                       if (c.getDescrip().equals(cuadruplosString.get(ic).getOp1S())){
                                           int tipo = c.getTipo();
                                           switch (tipo){
                                               case 903:
                                                   if (c.getValor().equals("")){
                                                       op1=Float.parseFloat(c.getDescrip());
                                                   } else op1=Float.parseFloat(c.getValor());
                                                   break;
                                               case 904:
                                                   if (c.getValor().equals("")){
                                                       op1=Float.parseFloat(c.getDescrip());
                                                   } else op1=Float.parseFloat(c.getValor());
                                                   break;
                                               default:
                                                   break;
                                           }
                                       }
                                   /*Sacar el operando 2*/
                                       if (c.getDescrip().equals(cuadruplosString.get(ic).getOp2S())){
                                           int tipo = c.getTipo();
                                           switch (tipo){
                                               case 903:
                                                   if (c.getValor().equals("")){
                                                       op2=Float.parseFloat(c.getDescrip());
                                                   } else op2=Float.parseFloat(c.getValor());
                                                   break;
                                               case 904:
                                                   if (c.getValor().equals("")){
                                                       op2=Float.parseFloat(c.getDescrip());
                                                   } else op2=Float.parseFloat(c.getValor());
                                                   break;
                                               default:
                                                   break;
                                           }
                                       }
                                   }
                                   float res = op1 * op2;
                                   for (Simbolo s : tabla_constantes){
                                       if (s.getDescrip().equals(cuadruplosString.get(ic).getResS())){
                                           s.setValor(String.valueOf(res));
                                       }
                                   }
                               }

                               break;

                           // *** DIVISION ***
                           case 935:
                               /*Revisar si es constante o simbolo*/
                               if (codigoOperacion == 935){
                                   float op1=0,op2=0;
                                   for (Simbolo s : tabla_simbolos){
                                   /*Sacar el operando 1*/
                                       if (s.getDescrip().equals(cuadruplosString.get(ic).getOp1S())){
                                           int tipo = s.getTipo();
                                           switch (tipo){
                                               case 903:
                                                   op1=Float.parseFloat(s.getValor());
                                                   break;
                                               case 904:
                                                   op1=Float.parseFloat(s.getValor());
                                                   break;
                                               default:
                                                   break;
                                           }
                                       }
                                   /*Sacar el operando 2*/
                                       if (s.getDescrip().equals(cuadruplosString.get(ic).getOp2S())){
                                           int tipo = s.getTipo();
                                           switch (tipo){
                                               case 903:
                                                   op2=Float.parseFloat(s.getValor());
                                                   break;
                                               case 904:
                                                   op2=Float.parseFloat(s.getValor());
                                                   break;
                                               default:
                                                   break;
                                           }
                                       }
                                   }

                                   for (Simbolo c : tabla_constantes){
                                   /*Sacar el operando 1*/
                                       if (c.getDescrip().equals(cuadruplosString.get(ic).getOp1S())){
                                           int tipo = c.getTipo();
                                           switch (tipo){
                                               case 903:
                                                   if (c.getValor().equals("")){
                                                       op1=Float.parseFloat(c.getDescrip());
                                                   } else op1=Float.parseFloat(c.getValor());
                                                   break;
                                               case 904:
                                                   if (c.getValor().equals("")){
                                                       op1=Float.parseFloat(c.getDescrip());
                                                   } else op1=Float.parseFloat(c.getValor());
                                                   break;
                                               default:
                                                   break;
                                           }
                                       }
                                   /*Sacar el operando 2*/
                                       if (c.getDescrip().equals(cuadruplosString.get(ic).getOp2S())){
                                           int tipo = c.getTipo();
                                           switch (tipo){
                                               case 903:
                                                   if (c.getValor().equals("")){
                                                       op2=Float.parseFloat(c.getDescrip());
                                                   } else op2=Float.parseFloat(c.getValor());
                                                   break;
                                               case 904:
                                                   if (c.getValor().equals("")){
                                                       op2=Float.parseFloat(c.getDescrip());
                                                   } else op2=Float.parseFloat(c.getValor());
                                                   break;
                                               default:
                                                   break;
                                           }
                                       }
                                   }
                                   float res = op1 / op2;
                                   for (Simbolo s : tabla_constantes){
                                       if (s.getDescrip().equals(cuadruplosString.get(ic).getResS())){
                                           s.setValor(String.valueOf(res));
                                       }
                                   }
                               }

                               break;

                           // *** OR ***
                           case 917:
                                /*Revisar si es constante o simbolo*/
                               if (codigoOperacion == 917){
                                   boolean op1=false,op2=false;
                                   for (Simbolo s : tabla_simbolos){
                                   /*Sacar el operando 1*/
                                       if (s.getDescrip().equals(cuadruplosString.get(ic).getOp1S())){
                                           int tipo = s.getTipo();
                                           switch (tipo){
                                               case 907:
                                                   if (s.getValor().equals("TRUE")){
                                                       op1 = true;
                                                   } else op1=false;
                                                   break;
                                               default:
                                                   break;
                                           }
                                       }
                                   /*Sacar el operando 2*/
                                       if (s.getDescrip().equals(cuadruplosString.get(ic).getOp2S())){
                                           int tipo = s.getTipo();
                                           switch (tipo){
                                               case 907:
                                                   if (s.getValor().equals("TRUE")){
                                                       op2 = true;
                                                   } else op2=false;
                                                   break;
                                               default:
                                                   break;
                                           }
                                       }
                                   }

                                   for (Simbolo c : tabla_constantes){
                                   /*Sacar el operando 1*/
                                       if (c.getDescrip().equals(cuadruplosString.get(ic).getOp1S())){
                                           int tipo = c.getTipo();
                                           switch (tipo){
                                               case 907:
                                                   if (c.getValor().equals("")){
                                                       if (c.getDescrip().equals("TRUE")){
                                                           op1 = true;
                                                       } else op1=false;
                                                   } else
                                                   if (c.getValor().equals("TRUE")){
                                                       op1 = true;
                                                   } else op1=false;
                                                   break;
                                               default:
                                                   break;
                                           }
                                       }
                                   /*Sacar el operando 2*/
                                       if (c.getDescrip().equals(cuadruplosString.get(ic).getOp2S())){
                                           int tipo = c.getTipo();
                                           switch (tipo){
                                               case 907:
                                                   if (c.getValor().equals("")){
                                                       if (c.getDescrip().equals("TRUE")){
                                                           op2 = true;
                                                       } else op2=false;
                                                   } else
                                                   if (c.getValor().equals("TRUE")){
                                                       op2 = true;
                                                   } else op2=false;
                                                   break;
                                               default:
                                                   break;
                                           }
                                       }
                                   }
                                   boolean res = op1 || op2;
                                   String valor;
                                   if (res){
                                       valor= "TRUE";
                                   } else valor = "FALSE";
                                   for (Simbolo s : tabla_constantes){
                                       if (s.getDescrip().equals(cuadruplosString.get(ic).getResS())){
                                           s.setValor(valor);
                                       }
                                   }
                               }
                               break;

                           // *** AND ***
                           case 918:
                               /*Revisar si es constante o simbolo*/
                               if (codigoOperacion == 918){
                                   boolean op1=false,op2=false;
                                   for (Simbolo s : tabla_simbolos){
                                   /*Sacar el operando 1*/
                                       if (s.getDescrip().equals(cuadruplosString.get(ic).getOp1S())){
                                           int tipo = s.getTipo();
                                           switch (tipo){
                                               case 907:
                                                   if (s.getValor().equals("TRUE")){
                                                       op1 = true;
                                                   } else op1=false;
                                                   break;
                                               default:
                                                   break;
                                           }
                                       }
                                   /*Sacar el operando 2*/
                                       if (s.getDescrip().equals(cuadruplosString.get(ic).getOp2S())){
                                           int tipo = s.getTipo();
                                           switch (tipo){
                                               case 907:
                                                   if (s.getValor().equals("TRUE")){
                                                       op2 = true;
                                                   } else op2=false;
                                                   break;
                                               default:
                                                   break;
                                           }
                                       }
                                   }

                                   for (Simbolo c : tabla_constantes){
                                   /*Sacar el operando 1*/
                                       if (c.getDescrip().equals(cuadruplosString.get(ic).getOp1S())){
                                           int tipo = c.getTipo();
                                           switch (tipo){
                                               case 907:
                                                   if (c.getValor().equals("")){
                                                       if (c.getDescrip().equals("TRUE")){
                                                           op1 = true;
                                                       } else op1=false;
                                                   } else
                                                   if (c.getValor().equals("TRUE")){
                                                       op1 = true;
                                                   } else op1=false;
                                                   break;
                                               default:
                                                   break;
                                           }
                                       }
                                   /*Sacar el operando 2*/
                                       if (c.getDescrip().equals(cuadruplosString.get(ic).getOp2S())){
                                           int tipo = c.getTipo();
                                           switch (tipo){
                                               case 907:
                                                   if (c.getValor().equals("")){
                                                       if (c.getDescrip().equals("TRUE")){
                                                           op2 = true;
                                                       } else op2=false;
                                                   } else
                                                   if (c.getValor().equals("TRUE")){
                                                       op2 = true;
                                                   } else op2=false;
                                                   break;
                                               default:
                                                   break;
                                           }
                                       }
                                   }
                                   boolean res = op1 && op2;
                                   String valor;
                                   if (res){
                                       valor= "TRUE";
                                   } else valor = "FALSE";
                                   for (Simbolo s : tabla_constantes){
                                       if (s.getDescrip().equals(cuadruplosString.get(ic).getResS())){
                                           s.setValor(valor);
                                       }
                                   }
                               }
                               break;

                           // *** NOT ***
                           case 919:
                               /*Revisar si es constante o simbolo*/
                               if (codigoOperacion == 919){
                                   boolean op1=false;
                                   for (Simbolo s : tabla_simbolos){
                                   /*Sacar el operando 1*/
                                       if (s.getDescrip().equals(cuadruplosString.get(ic).getOp1S())){
                                           int tipo = s.getTipo();
                                           switch (tipo){
                                               case 907:
                                                   if (s.getValor().equals("TRUE")){
                                                       op1 = true;
                                                   } else op1=false;
                                                   break;
                                               default:
                                                   break;
                                           }
                                       }
                                   }

                                   for (Simbolo c : tabla_constantes){
                                   /*Sacar el operando 1*/
                                       if (c.getDescrip().equals(cuadruplosString.get(ic).getOp1S())){
                                           int tipo = c.getTipo();
                                           switch (tipo){
                                               case 907:
                                                   if (c.getValor().equals("")){
                                                       if (c.getDescrip().equals("TRUE")){
                                                           op1 = true;
                                                       } else op1=false;
                                                   } else
                                                   if (c.getValor().equals("TRUE")){
                                                       op1 = true;
                                                   } else op1=false;
                                                   break;
                                               default:
                                                   break;
                                           }
                                       }
                                   }
                                   boolean res = !op1;
                                   String valor;
                                   if (res){
                                       valor= "TRUE";
                                   } else valor = "FALSE";
                                   for (Simbolo s : tabla_constantes){
                                       if (s.getDescrip().equals(cuadruplosString.get(ic).getResS())){
                                           s.setValor(valor);
                                       }
                                   }
                               }
                               break;



                           // *** ASIGNACION ***
                           case 926:
                               String valor = "";
                               for (Simbolo s : tabla_constantes){
                                   if (s.getDescrip().equals(cuadruplosString.get(ic).getOp1S())){
                                       if (s.getValor().equals("")){
                                          valor = s.getDescrip();
                                           //JOptionPane.showMessageDialog(null,"Encontre este valor: "+ valor);
                                       } else   {
                                           valor =s.getValor();
                                          // JOptionPane.showMessageDialog(null,"Encontre este valor: "+ valor);
                                       }

                                   }
                               }
                               for (Simbolo s : tabla_simbolos){
                                   if (s.getDescrip().equals(cuadruplosString.get(ic).getResS())){
                                       s.setValor(valor);
                                       //JOptionPane.showMessageDialog(null,"Ya le asigne: "+valor + "  a: "+s.getDescrip());
                                   }
                               }
                               for (Simbolo c : tabla_constantes){
                                   if (c.getDescrip().equals(cuadruplosString.get(ic).getResS())){
                                       c.setValor(valor);
                                      // JOptionPane.showMessageDialog(null,"Ya le asigne: "+valor + "  a: "+c.getDescrip());
                                   }
                               }
                               break;
                       }

                       // *** OP RELACIONALES >, >=, <, <= ***

                       if (codigoOperacion == 921 || codigoOperacion ==922 || codigoOperacion ==923 || codigoOperacion ==924){
                         /*Revisar si es constante o simbolo*/
                               float op1=0,op2=0;
                               for (Simbolo s : tabla_simbolos){
                                   /*Sacar el operando 1*/
                                   if (s.getDescrip().equals(cuadruplosString.get(ic).getOp1S())){
                                       int tipo = s.getTipo();
                                       switch (tipo){
                                           case 903:
                                               op1=Float.parseFloat(s.getValor());
                                               break;
                                           case 904:
                                               op1=Float.parseFloat(s.getValor());
                                               break;
                                           default:
                                               break;
                                       }
                                   }
                                   /*Sacar el operando 2*/
                                   if (s.getDescrip().equals(cuadruplosString.get(ic).getOp2S())){
                                       int tipo = s.getTipo();
                                       switch (tipo){
                                           case 903:
                                               op2=Float.parseFloat(s.getValor());
                                               break;
                                           case 904:
                                               op2=Float.parseFloat(s.getValor());
                                               break;
                                           default:
                                               break;
                                       }
                                   }
                               }

                               for (Simbolo c : tabla_constantes){
                                   /*Sacar el operando 1*/
                                   if (c.getDescrip().equals(cuadruplosString.get(ic).getOp1S())){
                                       int tipo = c.getTipo();
                                       switch (tipo){
                                           case 903:
                                               if (c.getValor().equals("")){
                                                   op1=Float.parseFloat(c.getDescrip());
                                               } else op1=Float.parseFloat(c.getValor());
                                               break;
                                           case 904:
                                               if (c.getValor().equals("")){
                                                   op1=Float.parseFloat(c.getDescrip());
                                               } else op1=Float.parseFloat(c.getValor());
                                               break;
                                           default:
                                               break;
                                       }
                                   }
                                   /*Sacar el operando 2*/
                                   if (c.getDescrip().equals(cuadruplosString.get(ic).getOp2S())){
                                       int tipo = c.getTipo();
                                       switch (tipo){
                                           case 903:
                                               if (c.getValor().equals("")){
                                                   op2=Float.parseFloat(c.getDescrip());
                                               } else op2=Float.parseFloat(c.getValor());
                                               break;
                                           case 904:
                                               if (c.getValor().equals("")){
                                                   op2=Float.parseFloat(c.getDescrip());
                                               } else op2=Float.parseFloat(c.getValor());
                                               break;
                                           default:
                                               break;
                                       }
                                   }
                               }
                           boolean res=false;
                           if (codigoOperacion == 921){
                               res = op1 < op2;
                           }
                           if (codigoOperacion == 922){
                               res = op1 <= op2;
                           }
                           if (codigoOperacion == 923){
                               res = op1 > op2;
                           }
                           if (codigoOperacion == 924){
                               res = op1 >= op2;
                           }
                               for (Simbolo s : tabla_constantes) {
                                   if (s.getDescrip().equals(cuadruplosString.get(ic).getResS())){
                                       if (res){
                                           s.setValor(String.valueOf("TRUE"));
                                       } else {
                                           s.setValor(String.valueOf("FALSE"));
                                       }

                                   }
                               }

                       }

                       // *** OP RELACIONALES !=, == ***

                       if (codigoOperacion ==920 || codigoOperacion ==931){
                         /*Revisar si es constante o simbolo*/
                           float op1=0,op2=0;
                           char ch1 = 'z', ch2 = 'z';
                           String st1 = "", st2 = "";
                           boolean b1=false, b2 = false;
                           int tipo1 = 0, tipo2 = 0;
                           for (Simbolo s : tabla_simbolos){
                                   /*Sacar el operando 1*/
                               if (s.getDescrip().equals(cuadruplosString.get(ic).getOp1S())){
                                   tipo1 = s.getTipo();
                                   switch (tipo1){
                                       case 903:
                                           op1=Float.parseFloat(s.getValor());
                                           break;
                                       case 904:
                                           op1=Float.parseFloat(s.getValor());
                                           break;
                                       case 905:
                                           ch1 = s.getValor().toCharArray()[0];
                                           break;
                                       case 906:
                                           st1 = s.getValor().substring(1,s.getValor().length()-1);
                                           break;
                                       case 907:
                                           if (s.getValor().equals("TRUE")){
                                              b1 = true;
                                           } else b1=false;
                                           break;
                                       default:
                                           break;
                                   }
                               }
                                   /*Sacar el operando 2*/
                               if (s.getDescrip().equals(cuadruplosString.get(ic).getOp2S())){
                                   tipo2 = s.getTipo();
                                   switch (tipo2){
                                       case 903:
                                           op2=Float.parseFloat(s.getValor());
                                           break;
                                       case 904:
                                           op2=Float.parseFloat(s.getValor());
                                           break;
                                       case 905:
                                           ch2 = s.getValor().toCharArray()[0];
                                           break;
                                       case 906:
                                           st2 = s.getValor().substring(1,s.getValor().length()-1);
                                           break;
                                       case 907:
                                           if (s.getValor().equals("TRUE")){
                                               b2 = true;
                                           } else b2=false;
                                           break;
                                       default:
                                           break;
                                   }
                               }
                           }

                           for (Simbolo c : tabla_constantes){
                                   /*Sacar el operando 1*/
                               if (c.getDescrip().equals(cuadruplosString.get(ic).getOp1S())){
                                   tipo1 = c.getTipo();
                                   switch (tipo1){
                                       case 903:
                                           if (c.getValor().equals("")){
                                               op1=Float.parseFloat(c.getDescrip());
                                           } else op1=Float.parseFloat(c.getValor());
                                           break;
                                       case 904:
                                           if (c.getValor().equals("")){
                                               op1=Float.parseFloat(c.getDescrip());
                                           } else op1=Float.parseFloat(c.getValor());
                                           break;
                                       case 905:
                                           ch1 = c.getDescrip().charAt(1); /*todo verificar esto*/
                                           break;
                                       case 906:
                                           st1 = c.getDescrip().substring(1,c.getValor().length()-1);
                                           break;
                                       case 907:
                                           if (c.getDescrip().equals("TRUE")){
                                               b1 = true;
                                           } else b1=false;
                                           break;
                                       default:
                                           break;
                                   }
                               }
                                   /*Sacar el operando 2*/
                               if (c.getDescrip().equals(cuadruplosString.get(ic).getOp2S())){
                                   tipo2 = c.getTipo();
                                   switch (tipo2){
                                       case 903:
                                           if (c.getValor().equals("")){
                                               op2=Float.parseFloat(c.getDescrip());
                                           } else op2=Float.parseFloat(c.getValor());
                                           break;
                                       case 904:
                                           if (c.getValor().equals("")){
                                               op2=Float.parseFloat(c.getDescrip());
                                           } else op2=Float.parseFloat(c.getValor());
                                           break;
                                       case 905:
                                           ch2 = c.getDescrip().charAt(1);
                                           break;
                                       case 906:
                                           st2 = c.getDescrip().substring(1,c.getValor().length()-1);
                                           break;
                                       case 907:
                                           if (c.getDescrip().equals("TRUE")){
                                               b2 = true;
                                           } else b2 = false;
                                           break;
                                       default:
                                           break;
                                   }
                               }
                           }
                           boolean res=false;
                           if (codigoOperacion == 920){
                               if (tipo1 == 903 && tipo2 == 903 ){
                                   res = op1 != op2;
                               }
                               if (tipo1 == 903 && tipo2 == 904 ){
                                   res = op1 != op2;
                               }
                               if (tipo1 == 904 && tipo2 == 903 ){
                                   res = op1 != op2;
                               }
                               if (tipo1 == 904 && tipo2 == 904 ){
                                   res = op1 != op2;
                               }
                               if (tipo1 == 905 && tipo2 == 905 ){
                                   res = ch1 != ch2;
                               }
                               if (tipo1 == 905 && tipo2 == 906 ){
                                   res = !st2.equals(ch1);
                               }
                               if (tipo1 == 906 && tipo2 == 905 ){
                                   res = !st1.equals(ch2);
                               }
                               if (tipo1 == 907 && tipo2 == 907 ){
                                   res = b1 != b2;
                               }

                           }
                           if (codigoOperacion == 931){
                               if (tipo1 == 903 && tipo2 == 903 ){
                                   res = op1 == op2;
                               }
                               if (tipo1 == 903 && tipo2 == 904 ){
                                   res = op1 == op2;
                               }
                               if (tipo1 == 904 && tipo2 == 903 ){
                                   res = op1 == op2;
                               }
                               if (tipo1 == 904 && tipo2 == 904 ){
                                   res = op1 == op2;
                               }
                               if (tipo1 == 905 && tipo2 == 905 ){
                                   res = ch1 == ch2;
                               }
                               if (tipo1 == 905 && tipo2 == 906 ){
                                   res = st2.equals(ch1);
                               }
                               if (tipo1 == 906 && tipo2 == 905 ){
                                   res = st1.equals(ch2);
                               }
                               if (tipo1 == 907 && tipo2 == 907 ){
                                   res = b1 == b2;
                               }
                           }

                           for (Simbolo s : tabla_constantes){
                               if (s.getDescrip().equals(cuadruplosString.get(ic).getResS())){
                                   if (res){
                                       s.setValor(String.valueOf("TRUE"));
                                   } else s.setValor(String.valueOf("FALSE"));

                               }
                           }

                       }

                   }


                   JOptionPane.showMessageDialog(null,"Terminó la ejecución");
                   termino=true;
               }
           } , "hiloEjecucion");
           hiloEjecucion.start();
       }
    }



    public static void main(String[] args) {
        Principal object = new Principal();
        //object.LoadMatrix();
        object.Frame();
    }
}
