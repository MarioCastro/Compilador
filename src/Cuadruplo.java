import java.io.Serializable;

/**
 * Created by SoOreck on 15/10/2016.
 */
public class Cuadruplo implements Serializable{

    /* La lista de cuadruplos sera  ArrayList<Cuadruplo> cuadruplos = new ArrayList<>(); y se agregaran
    * cuadruplos.add(new Cuadruplo().generarCuadruplo(codOper, op1, op2, res)*/

    private int codoper;
    private int op1;
    private int op2;
    private int res;
    private int apuntador;

    //Para la tabla
    private String codoperS;
    private String op1S;
    private String op2S;
    private String resS;

    public Cuadruplo(){

    }

    public int getApuntador() {
        return apuntador;
    }

    public void setApuntador(int apuntador) {
        this.apuntador = apuntador;
    }

    public int getCodoper() {
        return codoper;
    }

    public void setCodoper(int codoper) {
        this.codoper = codoper;
    }

    public int getOp1() {
        return op1;
    }

    public void setOp1(int op1) {
        this.op1 = op1;
    }

    public int getOp2() {
        return op2;
    }

    public void setOp2(int op2) {
        this.op2 = op2;
    }

    public int getRes() {
        return res;
    }

    public void setRes(int res) {
        this.res = res;
    }

    public void generarCuadruplo(int codoper, int op1, int op2, int res){
           this.codoper = codoper;
           this.op1 = op1;
           this.op2 = op2;
           this.res = res;
    }

    public String getCodoperS() {
        return codoperS;
    }

    public void setCodoperS(String codoperS) {
        this.codoperS = codoperS;
    }

    public String getOp1S() {
        return op1S;
    }

    public void setOp1S(String op1S) {
        this.op1S = op1S;
    }

    public String getOp2S() {
        return op2S;
    }

    public void setOp2S(String op2S) {
        this.op2S = op2S;
    }

    public String getResS() {
        return resS;
    }

    public void setResS(String resS) {
        this.resS = resS;
    }

    public void generarCuadruploString(String codoper, String op1, String op2, String res){
        this.codoperS = codoper;
        this.op1S = op1;
        this.op2S = op2;
        this.resS = res;
    }
}
