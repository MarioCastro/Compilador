import javax.swing.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by SoOreck on 27/10/2016.
 */
public class H implements Serializable {
    ArrayList<Simbolo> tabla_simbolos = new ArrayList<Simbolo>();
    ArrayList<Simbolo> tabla_constantes = new ArrayList<Simbolo>();
    ArrayList<Cuadruplo> cuadruplos = new ArrayList<Cuadruplo>();
    String codigoFuente;

    //Para mostrar
    public List<Simbolo> constantesTabla = new ArrayList<Simbolo>();
    public List<Simbolo> simbolosTabla = new ArrayList<Simbolo>();
    public List<Cuadruplo> cuadruplosTabla = new ArrayList<Cuadruplo>();

    Hashtable<String, H> table;
    String ruta = "Files\\";

    public H(){

    }

    public H(String nombreArchivo) throws
            IOException, ClassNotFoundException{
        this.ruta += nombreArchivo+".h";
        FileInputStream fi=null;
        ObjectInputStream oi= null;
        try{
            fi= new FileInputStream(ruta);
            oi=new ObjectInputStream(fi);
            table= (Hashtable) oi.readObject();
            oi.close();
        }
        catch (FileNotFoundException e){
            JOptionPane.showMessageDialog(null,"No existe");
            table= new Hashtable< String,H>();
        }
    }

    public ArrayList<Simbolo> getTabla_simbolos() {
        return tabla_simbolos;
    }

    public void setTabla_simbolos(ArrayList<Simbolo> tabla_simbolos) {
        this.tabla_simbolos = tabla_simbolos;
    }

    public ArrayList<Simbolo> getTabla_constantes() {
        return tabla_constantes;
    }

    public void setTabla_constantes(ArrayList<Simbolo> tabla_constantes) {
        this.tabla_constantes = tabla_constantes;
    }

    public ArrayList<Cuadruplo> getCuadruplos() {
        return cuadruplos;
    }

    public void setCuadruplos(ArrayList<Cuadruplo> cuadruplos) {
        this.cuadruplos = cuadruplos;
    }

    public String getCodigoFuente() {
        return codigoFuente;
    }

    public void setCodigoFuente(String codigoFuente) {
        this.codigoFuente = codigoFuente;
    }

    public List<Simbolo> getConstantesTabla() {
        return constantesTabla;
    }

    public void setConstantesTabla(List<Simbolo> constantesTabla) {
        this.constantesTabla = constantesTabla;
    }

    public List<Simbolo> getSimbolosTabla() {
        return simbolosTabla;
    }

    public void setSimbolosTabla(List<Simbolo> simbolosTabla) {
        this.simbolosTabla = simbolosTabla;
    }

    public List<Cuadruplo> getCuadruplosTabla() {
        return cuadruplosTabla;
    }

    public void setCuadruplosTabla(List<Cuadruplo> cuadruplosTabla) {
        this.cuadruplosTabla = cuadruplosTabla;
    }

    /*Metodos de archivos*/
    public boolean addH(ArrayList<Cuadruplo> cuadruplos, ArrayList<Simbolo> tabla_simbolos,
                        ArrayList<Simbolo> tabla_constantes, String codigoFuente){

        if (!table.containsKey("hpp"))
        {
            try
            {
                H hpp = new H();
                hpp.setCuadruplos(cuadruplos);
                hpp.setTabla_simbolos(tabla_simbolos);
                hpp.setTabla_constantes(tabla_constantes);
                hpp.setCodigoFuente(codigoFuente);
                hpp.setConstantesTabla(Principal.modeloTablaConstantes.regresa());
                hpp.setSimbolosTabla(Principal.modeloTablaSimbolos.regresa());
                hpp.setCuadruplosTabla(Principal.modeloTablaCuadruplos.regresa());
                table.put("hpp",hpp);
                guardar();
            }
            catch (IOException e1) { e1.printStackTrace(); }
            catch (ClassNotFoundException e1) { e1.printStackTrace(); }
            return true;
        }
        else {return false;}/*Agrega un nuevo objeto a la table*/
    }

    public boolean eliminar (String cl){

        if(table.containsKey(cl)){
            table.remove(cl);
            return true;
        }
        else{
            return false;
        }
    }      /*Elimina el objeto con la clave*/

    public H recuperar(String clave){
        if(table.containsKey(clave)){
            return table.get(clave);
        }
        else{
            return null;
        }
    }  /*Retorna el objeto con la clave*/

    public Enumeration totalU (){                 //Mostrar todos los usuarios
        return table.keys();
    }  /*Retorna un conjunto de todas las claves*/

    public void guardar () throws IOException,ClassNotFoundException {
        FileOutputStream fsalida=new FileOutputStream(ruta);
        ObjectOutputStream os=new ObjectOutputStream(fsalida);
        os.writeObject(table);
        os.close();
    }
}
